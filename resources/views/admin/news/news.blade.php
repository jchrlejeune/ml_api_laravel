@extends('admin.templateBack')

@section('content')

<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6">Liste des actualités</h1>
    <div class="mb-6">
        <a href="{{ route('admin.news.create') }}" class="bg-gray-700 hover:bg-gray-800 text-white font-semibold py-2 px-4 rounded-full inline-block transition duration-300">
            Créer Nouvel Article
        </a>
    </div>
    @if(session('success'))
    <div class="bg-green-700 text-white p-3 mb-4 rounded">
        {{ session('success') }}
    </div>
    @endif
    
    <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
        @foreach($news as $newsItem)
        <div class="bg-gray-800  hover:bg-gray-700 hover:text-white p-6 mb-4 border rounded-lg shadow-lg transition duration-300">
            <h3 class="text-xl font-semibold mb-2 text-white">{{ $newsItem->title }}</h3>
            <div class="flex items-center mb-4">
                @if($newsItem->image_url)
                <img src="{{ $newsItem->image_url }}" alt="{{ $newsItem->title }}" class="w-64 h-32 object-cover mr-4">
                @else
                <div class="w-12 h-12 bg-gray-600 rounded-full flex items-center justify-center mr-4">
                    <i class="fas fa-image text-gray-400"></i>
                </div>
                @endif
            </div>
            <div>
                <p class="text-gray-400 mb-4 px-2 py-3 overflow-hidden break-words">
                    {{ \Illuminate\Support\Str::limit($newsItem->description, 100, $end='...') 
                }}</p>
            </div>
            @if ($newsItem->hashtag)
                {{-- Diviser la chaîne de hashtags en un tableau --}}
                <?php $hashtagsArray = explode(', ', $newsItem->hashtag); ?>

                {{-- Afficher les hashtags avec un dièse (#) --}}
                @foreach ($hashtagsArray as $hashtag)
                    @if($loop->iteration <= 4)
                        <span class="text-gray-500 mb-2">
                            #{{ \Illuminate\Support\Str::limit($hashtag, 8, $end='...')}}
                            @if(!$loop->last),@endif
                        </span>
                    @endif
                @endforeach

            @endif
            <p class="text-gray-500 mb-2"><i class="fas fa-map-marker-alt mr-2"></i>{{ $newsItem->place }}</p>
            <div class="flex items-center">
                <a href="{{ route('admin.news.edit', $newsItem->id) }}" class="text-blue-500 hover:underline mr-4">
                    <i class="fas fa-edit mr-1"></i>
                </a>
                <form action="{{ route('admin.news.destroy', $newsItem->id) }}" method="POST" class="inline form-delete">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="text-red-500 hover:underline">
                        <i class="fas fa-trash-alt mr-1"></i>
                    </button>
                </form>
            </div>
        </div>
        @endforeach

    </div>
    <div className="pt-4 bg-gray-900 text-white">
        {{ $news->links() }}
    </div>
    
</div>



@endsection

