@extends('admin.templateBack')

@section('content')

<div class="container mx-auto w-full lg:w-1/2 mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h2 class="text-xl font-semibold mb-4">Modifier l'Article</h2>
    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif
    <!-- Formulaire d'édition -->
    <form action="{{ route('admin.news.update', $news->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <input type="hidden" name="user_id" value="{{ $news->user_id }}">


        <div class="mb-4">
            <label for="title" class="block text-sm font-medium text-gray-600">Titre :</label>
            <input type="text" id="title" name="title" required value="{{ $news->title }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="description" class="block text-sm font-medium text-gray-600">Description :</label>
            <textarea id="description" name="description" required
                      class="mt-1 p-2 w-full h-40 bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ $news->description }}</textarea>
        </div>
        

        <div class="mb-4">
            <label for="place" class="block text-sm font-medium text-gray-600">Lieu :</label>
            <input type="text" id="lieu" name="place" required value="{{ $news->place }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <div class="mb-4">
                <label for="hashtag" class="block text-sm font-medium text-gray-400">Hashtag | Séparer les mots par une virgule " , ":</label>
                <input type="hashtag" id="place" name="hashtag" value="{{ $news->hashtag }}"
                    class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
            </div>
        </div>

        <div class="mb-4">
            <label for="current_picture" class="block text-sm font-medium text-gray-600">Photo actuelle :</label>
            @if($news->image_path)
            <img src="{{ asset($news->image_url) }}" alt="{{ $news->image_title }}"
                class="mt-1 p-2 w-32 h-32 object-cover border rounded-md">
            @else
            Pas d'image actuelle
            @endif
        </div>
        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium text-gray-600">Photo :</label>
            <input type="file" id="image_path" name="image_path" accept="image/jpeg"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="flex  justify-center my-4">
            <button type="submit"
                class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Enregistrer les Modifications
            </button>
        </div>
    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('admin.news.news') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>


@endsection