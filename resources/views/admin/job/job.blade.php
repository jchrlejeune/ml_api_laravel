@extends('admin.templateBack')

@section('content')



<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <div class="my-8 flex justify-between items-center">
        <h2 class="text-xl font-semibold mb-4">Offre(s) D'emploi à Valider</h2>
        <a href="{{ route('admin.job.create') }}" class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Nouveau
        </a>
    </div>
    @if(session('success'))
    <div class="bg-green-600 text-white p-4 rounded mb-4">
        {{ session('success') }}
    </div>
@endif
@if(session('error'))
<div class="bg-red-600 text-white p-4 rounded mb-4">
    {{ session('error') }}
</div>
@endif
    <div class="overflow-x-auto">
        <table  class="min-w-full bg-gray-800 border border-gray-600 rounded">
            <thead>
                <tr>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Entreprise</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Titre</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Job</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Type</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Description</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">image</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">PDF</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Date de publication</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Valider</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Editer</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Supprimer</th>
                </tr>
            </thead>
            <tbody>
                @foreach($inactiveJob as $job)
                <tr>
                    <td class="py-4 px-6 border-t">{{ $job->entreprise }}</td>
                    <td class="py-4 px-6 border-t">{{ $job->title }}</td>
                    <td class="py-4 px-6 border-t">{{ $job->job }}</td>
                    <td class="py-4 px-6 border-t">{{ $job->type }}</td>
                    <td class="py-4 px-6 border-t">{{ $job->description }}</td>
                    <td class="py-4 px-6 border-t">
                        @if($job->image_path)
                            <img src="{{ $job->image_url}}" alt="{{ $job->title }}" class="w-40 h-20 object-cover">
                        @else
                            Pas d'image
                        @endif
                    </td>
                    <td class="py-4 px-6 border-t">
                        @if($job->job_pdf)
                        <a href="{{ asset($job->job_url) }}" download="{{ $job->title }}.pdf">Télécharger le PDF</a>
                        @else
                            Pas de fichier Pdf
                        @endif
                    </td>
                    <td class="py-4 px-6 border-t">{{ \Carbon\Carbon::parse($job->publication)->format('d/m/Y') }}</td>
                
                    <td class="py-4 px-6 border-t">
                        <a href="{{ route('admin.job.toggle', $job->id) }}" class="text-green-400 hover:text-green-200"><i class="fa-solid fa-check"></i></a>
                        
                    </td>
                    <td class="py-4 px-6 border-t">
                        <a href="{{ route('admin.job.edit_job', $job->id) }}"class="text-blue-400 hover:text-blue-200"><i class="fa-solid fa-pen"></i></a>
                        
                    </td>
                    <td class="py-4 px-6 border-t">
                        <form action="{{ route('admin.job.delete', $job->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="text-red-400 hover:text-green-200"><i class="fa-solid fa-trash"></i></button>
                        </form>
                    </td>


                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



@endsection

