@extends('admin.templateBack')

@section('content')

<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">


    <div class="my-8 flex justify-between items-center">
        <h2 class="text-xl font-semibold mb-4">Membres du Personnel</h2>
        <a href="{{ route('admin.staff.create_staff') }}" class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Nouveau
        </a>
    </div>

    @if (session('success'))
        <div class="bg-green-700 text-white p-3 mb-4 rounded">
            {{ session('success') }}
        </div>
    @endif

    @foreach($staffGroups as $group => $staffMembers)
        <h3 class="text-lg font-semibold mb-4 mt-2">{{ $group }}</h3>
        <div class="overflow-x-auto mb-8">
            <table class="min-w-full bg-gray-800 border border-gray-600 rounded">
                <!-- Entête du tableau -->
                <thead>
                    <tr>
                        <th class="text-left py-3 px-4 bg-gray-700">Prénom</th>
                        <th class="text-left py-3 px-4 bg-gray-700">Nom</th>
                        <th class="text-left py-3 px-4 bg-gray-700">Poste</th>
                        <th class="text-left py-3 px-4 bg-gray-700">Image</th>
                        <th class="text-left py-3 px-4 bg-gray-700">Editer</th>
                        <th class="text-left py-3 px-4 bg-gray-700">Supprimer</th>
                    </tr>
                </thead>
                <!-- Corps du tableau -->
                <tbody>
                    @foreach($staffMembers as $staff)
                        <tr>
                            <td class="border-t py-4 px-4">{{ $staff->first_name }}</td>
                            <td class="border-t py-4 px-4">{{ $staff->last_name }}</td>
                            <td class="border-t py-4 px-4">{{ $staff->job }}</td>
                            <td class="border-t py-4 px-4">
                                @if($staff->picture)
                                    <img src="{{ $staff->image_url}}" alt="{{ $staff->first_name }}" class="w-32 h-32 object-cover">
                                @else
                                    Pas d'image
                                @endif
                            </td>
                            <td class="border-t py-4 px-4">
                                <a href="{{ route('admin.staff.staff.edit', $staff->id) }}" class="text-blue-500 hover:underline mr-2"><i class="fa-solid fa-user-pen"></i></a>
                                
                            </td>
                            <td class="border-t py-4 px-4">
                                <form action="{{ route('admin.staff.delete', $staff->id) }}" method="POST" class="inline form-delete">
                                    @csrf
                                    @method('DELETE')
                                    <button type="button" class="text-red-500 hover:underline delete-button">
                                        <i class="fa-solid fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endforeach

</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('.delete-button').on('click', function() {
            var form = $(this).closest('form');
            if (confirm('Êtes-vous sûr de vouloir supprimer ce membre du personnel ? Cette action est irréversible.')) {
                form.submit();
            }
        });
    });
</script>

@endsection
