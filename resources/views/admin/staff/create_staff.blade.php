@extends('admin.templateBack')

@section('content')
<div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">

    <h2 class="text-2xl font-semibold mb-6 text-center">Créer un Membre du Personnel</h2>

    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('admin.staff.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="mb-4">
            <label for="first_name" class="block text-sm font-medium mb-1">Prénom :</label>
            <input type="text" id="first_name" name="first_name" required
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="last_name" class="block text-sm font-medium mb-1">Nom :</label>
            <input type="text" id="last_name" name="last_name" required
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job" class="block text-sm font-medium mb-1">Job :</label>
            <input type="text" id="job" name="job" required
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="picture" class="block text-sm font-medium mb-1">Photo | *.jpg :</label>
            <input type="file" id="picture" name="picture" accept="image/jpeg"
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="group" class="block text-sm font-medium mb-1">Groupe :</label>
            <select id="group" name="group" required
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
                <option value="Conseil d'administration">Conseil d'administration</option>
                <option value="Bureau">Bureau</option>
                <option value="Pôle Direction">Pôle Direction</option>
                <option value="Pôle Structure">Pôle Structure</option>
                <option value="Pôle Technique">Pôle Technique</option>
            </select>
        </div>

        <div class="flex  justify-center my-4">
            <button type="submit"
                class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Enregistrer les Modifications
            </button>
        </div>
    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('admin.staff.staff') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>
@endsection