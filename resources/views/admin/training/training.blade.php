@extends('admin.templateBack')

@section('content')



<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <div class="my-8 flex justify-between items-center">
        <h2 class="text-xl font-semibold mb-4">Formations</h2>
        <a href="{{ route('admin.training.create') }}" class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Nouveau
        </a>
        <a href="{{ route('admin.training.traininginactive') }}" class="bg-red-700 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Formations Inactive
        </a>
    </div>
    @if(session('success'))
    <div class="bg-green-600 text-white p-4 rounded mb-4">
        {{ session('success') }}
    </div>
@endif
@if(session('error'))
<div class="bg-red-600 text-white p-4 rounded mb-4">
    {{ session('error') }}
</div>
@endif
    <div class="overflow-x-auto">
        <table  class="min-w-full bg-gray-800 border border-gray-600 rounded">
            <thead>
                <tr>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Titre</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Job</th>
                    {{-- <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Description</th> --}}
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Image_path</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Date de publication</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Date de début</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Date de fin</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Editer</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Cacher</th>
                </tr>
            </thead>
            <tbody classNAme="mb-4">
                
                @foreach($activeTrainings as $training)
                <tr>
                    <td class="py-4 px-6 border-t">{{ $training->title }}</td>
                    <td class="py-4 px-6 border-t">{{ Illuminate\Support\Str::limit($training->job, 40) }}</td>
                    {{-- <td class="py-4 px-6 border-t">{{ Illuminate\Support\Str::limit($training->description, 20) }}</td> --}}

                    <td class="py-4 px-6 border-t">
                        @if($training->image_path)
                            <img src="{{$training->image_url}}" alt="{{ $training->title }}" class="w-40 h-20 object-cover">
                        @else
                            Pas d'image
                        @endif
                    </td>
                    <td class="py-4 px-6 border-t">{{ \Carbon\Carbon::parse($training->publication)->format('d/m/Y') }}</td>
                    <td class="py-4 px-6 border-t">{{ \Carbon\Carbon::parse($training->start)->format('d/m/Y') }}</td>
                    <td class="py-4 px-6 border-t">{{ \Carbon\Carbon::parse($training->end)->format('d/m/Y') }}</td>
                    
                    <td class="py-4 px-6 border-t">
                        <a href="{{ route('admin.training.edit_training', $training->id) }}" class="text-blue-400 hover:text-blue-200 mr-2"><i class="fas fa-edit"></i></a>
                    </td>
                    <td class="py-4 px-6 border-t">
                        <a href="{{ route('admin.training.toggle', $training->id) }}" class="text-green-400 hover:text-green-200"><i class="fas fa-lock-open"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div className="pt-4 my-4 bg-gray-900 text-white">
        {{ $activeTrainings->links() }}
    </div>
</div>


@endsection

