@extends('admin.templateBack')

@section('content')

<div class="container mx-auto w-full lg:w-1/2 mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Créer une Offre de Formation</h1>
    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('admin.training.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="mb-4">
            <label for="title" class="block text-sm font-medium">Titre :</label>
            <input type="text" id="title" name="title" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job" class="block text-sm font-medium">Job visé :</label>
            <textarea id="job" name="job" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>

        <div class="mb-4">
            <label for="type" class="block text-sm font-medium">Type de formation :</label>
            <select id="type" name="type" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
                <option value="Continue">Continue</option>
                <option value="Qualifiante">Qualifiante</option>
                <option value="Alternance">Alternance</option>
                <option value="Certifiante">Certifiante</option>
            </select>
        </div>

        <div class="mb-4">
            <label for="description" class="block text-sm font-medium">Description :</label>
            <textarea id="description" name="description" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>

        <div class="mb-4">
            <label for="publication" class="block text-sm font-medium">Date de Publication :</label>
            <input type="date" id="publication" name="publication" value="{{ now()->format('Y-m-d') }}" readonly
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="start" class="block text-sm font-medium">Date de début :</label>
            <input type="date" id="start" name="start" min="{{ now()->format('Y-m-d') }}"
            class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="end" class="block text-sm font-medium">Date de fin :</label>
            <input type="date" id="end" name="end" min="{{ now()->format('Y-m-d') }}"
            class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="slot" class="block text-sm font-medium">Nombre de place :</label>
            <input type="text" id="slot" name="slot" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
      
        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium text-gray-400">Image :</label>
            <input type="file" id="image_path" name="image_path" accept="image/*"  class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

    
        <div class="mb-4">
            <label for="place" class="block text-sm font-medium">Lieu :</label>
            <input type="text" id="place" name="place" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
    
        <div class="mb-4">
            <label for="job_summary" class="block text-sm font-medium">Résumé du métier :</label>
            <textarea id="job_summary" name="job_summary" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>

        <div class="mb-4">
            <label for="objectives" class="block text-sm font-medium">Objectif :</label>
            <textarea id="objectives" name="objectives" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>
        <div class="mb-4">
            <label for="duration" class="block text-sm font-medium">Durée :</label>
            <textarea id="duration" name="duration" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>
        <div class="mb-4">
            <label for="prerequisites" class="block text-sm font-medium">Pré-requis :</label>
            <textarea id="prerequisites" name="prerequisites" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>
        <div class="mb-4">
            <label for="program" class="block text-sm font-medium">Programme :</label>
            <textarea id="program" name="program" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>
    
        <!-- Champ caché pour l'id de l'utilisateur connecté -->
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
        
        <div class="flex justify-center mb-4">
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue w-full">
                Créer Offre de Formation
            </button>
        </div>
    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('admin.training.training') }}" class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>

@endsection