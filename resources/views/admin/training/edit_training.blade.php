@extends('admin.templateBack')

@section('content')

<div class="container mx-auto w-full lg:w-1/2 mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h2 class="text-xl font-semibold mb-4">Modifier une Formation</h2>

    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('admin.training.update', $training->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="mb-4">
            <label for="title" class="block text-sm font-medium">Titre :</label>
            <input type="text" id="title" name="title" value="{{ old('title', $training->title) }}" required
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job" class="block text-sm font-medium">Job :</label>
            <input type="text" id="job" name="job" value="{{ old('job', $training->job) }}" required
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="type" class="block text-sm font-medium">Type de formation :</label>
            <select id="type" name="type"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
                <option value="Continue" {{ old('type', $training->type) === 'Continue' ? 'selected' : '' }}>Continue
                </option>
                <option value="Qualifiante" {{ old('type', $training->type) === 'Qualifiante' ? 'selected' : ''
                    }}>Qualifiante</option>
                <option value="Alternance" {{ old('type', $training->type) === 'Alternance' ? 'selected' : ''
                    }}>Alternance</option>
                <option value="Certifiante" {{ old('type', $training->type) === 'Certifiante' ? 'selected' : ''
                    }}>Certifiante</option>
            </select>
        </div>
        <div class="mb-4">
            <label for="description" class="block text-sm font-medium">Description :</label>
            <textarea id="description" name="description" required rows="4"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ old('description', $training->description) }}</textarea>
        </div>

        <div class="mb-4">
            <label for="start" class="block text-sm font-medium">Date de début :</label>
            <input type="date" id="start" name="start" value="{{ old('start', $training->start) }}" required
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="end" class="block text-sm font-medium">Date de fin :</label>
            <input type="date" id="end" name="end" min="{{ now()->format('Y-m-d') }}" value="{{ old('start', $training->end) }}" required
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="slots" class="block text-sm font-medium">Nombre de place :</label>
            <input type="text" id="slots" name="slots" min="{{ now()->format('Y-m-d') }}" value="{{ old('slot', $training->slots) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="current_picture" class="block text-sm font-medium text-gray-600">Photo actuelle :</label>
            @if($training->image_path)
            <img src="{{ $training->image_url }}"
                class="mt-1 p-2 w-32 h-32 object-cover border rounded-md">
            @else
            Pas d'image actuelle
            @endif
        </div>
        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium text-gray-600">Photo :</label>
            <input type="file" id="image_path" name="image_path"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>


        <div class="mb-4">
            <label for="place" class="block text-sm font-medium">Lieu :</label>
            <input type="text" id="place" name="place" value="{{ old('place', $training->place) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job_summary" class="block text-sm font-medium">Résumé du métier :</label>
            <input id="job_summary" name="job_summary" value="{{ old('job_summary', $training->job_summary) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="objectives" class="block text-sm font-medium">Objectif :</label>
            <input id="objectives" name="objectives" value="{{ old('objectives', $training->objectives) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="duration" class="block text-sm font-medium">Durée :</label>
            <input id="duration" name="duration" value="{{ old('duration', $training->duration) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="prerequisites" class="block text-sm font-medium">Pré-requis :</label>
            <input id="prerequisites" name="prerequisites" value="{{ old('prerequisites', $training->prerequisites) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="program" class="block text-sm font-medium">Programme :</label>
            <input id="program" name="program" value="{{ old('program', $training->program) }}"
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <!-- Champ caché pour l'id de l'utilisateur connecté -->
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

        <div class="flex justify-center my-4">
            <button type="submit"
                class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Enregistrer les Modifications
            </button>
        </div>

    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('admin.training.training') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>

@endsection