@extends('admin.templateBack')

@section('content')
    <div class="space-y-4">
        
        @foreach ($notificationDetails as $notificationDetail)
            <div class="bg-white border rounded-md shadow-md p-4 flex flex-col gap-2">
                @if($notificationDetail['jeune'])
                    <div class="flex items-center">
                        <h4 class="text-lg font-bold mr-2">Jeune:</h4>
                        <p>Nom : {{ $notificationDetail['jeune']->last_name }}</p>
                        <p>Prénom : {{ $notificationDetail['jeune']->first_name }}</p>
                        <p>Email : {{ $notificationDetail['jeune']->email }}</p>
                        <!-- Ajoutez d'autres détails du jeune ici -->
                    </div>
                @endif
                @if($notificationDetail['offre'])
                    <div class="flex items-center mt-2">
                        <h4 class="text-lg font-bold mr-2">Offre:</h4>
                        <p>Titre : {{ $notificationDetail['offre']->title }}</p>
                        <p>Description : {{ $notificationDetail['offre']->description }}</p>
                        <!-- Ajoutez d'autres détails de l'offre ici -->
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@endsection