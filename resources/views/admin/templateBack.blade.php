<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mission Locale</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Source+Serif+Pro:ital,wght@1,300&display=swap"
    rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        /* Styles pour le titre dans l'en-tête en mode mobile */
        @media (min-width: 640px) {
            header {
                display: none;
            }
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="{{asset("js/moment.js")}}"></script>
</head>

<body class="bg-gray-500 font-sans">
    <div class="flex flex-col lg:flex-row bg-gray-200 min-h-screen">
        
        <!-- Menu latéral agrandi -->
        <div class="bg-gray-800 lg:w-64 flex flex-col shadow-lg ">
            <h1 class="text-3xl font-bold text-white  py-2 px-6 my-3">
                <img src="/Images/logo-inscription.png" alt="logo" class="my-2">
                <span class="my-2 py-2">Mission Locale</span>
            </h1>
            <nav>
                @if(auth()->user()->role === 'admin')
                <ul>
                    <li>
                        <span class="font-bold text-xl flex items-center py-3 px-6 text-white">
                            <i class="fa-solid fa-circle-user mr-3"></i>
                            Admin
                        </span>
                    </li>
                    <li>
                        <a href="{{ route('admin.manager.manager') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-user-tie mr-3"></i>
                            Conseiller
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.businessManager.manager') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-user-tie mr-3"></i>Chargé d'entreprises
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.user.user') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fas fa-users mr-3"></i>Jeune
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.validPreregister') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fas fa-check-circle mr-3"></i>
                            Validation Jeune
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.company.company') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fas fa-building mr-3"></i>
                            Entreprise
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.validPreregisterEntreprise') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fas fa-check-circle mr-3"></i>
                            Validation Entreprise
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.staff.staff') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-image-portrait mr-3"></i>
                            Notre Équipe
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.photogroupe.photogroupe') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-people-group mr-3"></i>
                            Photo d'équipe
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.news.news') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fas fa-newspaper mr-3"></i>
                            Actualités
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.training.training') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-graduation-cap mr-3"></i>
                            Formation
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.atelier.atelier') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-clipboard mr-3"></i>
                            Atelier
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.job.job') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-briefcase mr-3"></i>
                            Emploi Non Validé
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.job.alljob') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fa-solid fa-briefcase mr-3"></i>
                            Emploi Validé
                        </a>
                    </li>

                    <li class="my-2">
                        <a href="{{ route('admin.logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                            class="text-red-500 hover:bg-red-950 transition duration-300 flex items-center py-3 px-6 ">
                            <i class="fa-solid fa-right-from-bracket  mr-3"></i>
                            Déconnexion
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </ul>


                @elseif(auth()->user()->role === 'manager')
                    <ul>
                        <li>
                            <p  class="font-bold  flex items-center py-3 px-6 text-white"><i class="fa-solid fa-circle-user mr-3"></i> {{ auth()->user()->information->first_name }}</p>
                        </li>
                        <li>
                            <a href="{{ route('conseiller.user.user') }}"
                            class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                            <i class="fas fa-users mr-3"></i>
                            Jeune
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('conseiller.notification.index') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fas fa-tachometer-alt mr-3"></i>
                                Notifications
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('conseiller.appointments.calendar') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fa-regular fa-calendar mr-3"></i>
                                Mon calendrier
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('conseiller.trainings.training') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fas fa-chalkboard mr-3"></i>
                                Formations
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('conseiller.ateliers.atelier') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fa-solid fa-clipboard mr-3"></i>
                                Ateliers
                            </a>
                        </li>

                        <li class="my-2">
                            <a href="{{ route('admin.logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                class="text-red-500 hover:bg-red-950 transition duration-300 flex items-center py-3 px-6 ">
                                <i class="fa-solid fa-right-from-bracket  mr-3"></i>
                                Déconnexion
                            </a>
                        </li>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                    
                @elseif(auth()->user()->role === 'businessManager')

                    <ul>
                        <li>
                            <p  class="font-bold  flex items-center py-3 px-6 text-white">
                                <i class="fa-solid fa-circle-user mr-3"></i> {{ auth()->user()->information->first_name }}</p>
                        </li>
                        <li>
                            <a href="{{ route('businessManager.entreprise') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fas fa-users mr-3"></i>
                                Mes Entreprises
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('businessManager.job.job') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fa-solid fa-briefcase mr-3"></i>
                                Emploi Non Validé
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('businessManager.job.alljob') }}"
                                class="flex items-center py-3 px-6 text-gray-300 hover:bg-gray-700 hover:text-white">
                                <i class="fa-solid fa-briefcase mr-3"></i>
                                Emploi Validé
                            </a>
                        </li>

                        <li class="my-2">
                            <a href="{{ route('admin.logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                class="text-red-500 hover:bg-red-950 transition duration-300 flex items-center py-3 px-6 ">
                                <i class="fa-solid fa-right-from-bracket  mr-3"></i>
                                Déconnexion
                            </a>
                        </li>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                    
                @endif
            </nav>
        </div>

        <div class="flex-1 flex flex-col">
            <div class="bg-gray-800"> <!-- lg:hidden  -->
                <!-- En-tête avec le titre -->
                <button id="toggleMenu" class=" text-end block w-full py-3 px-6 text-gray-300 hover:bg-gray-700 focus:outline-none focus:bg-gray-700">
                    <i class="fa-solid fa-bars mr-2"></i> Menu
                </button>
            </div>
            <!-- Contenu de la page -->
            <main class="px-2 flex-1 flex justify-center  bg-gray-500">
                @if(auth()->user()->role === 'admin')
                        @yield('content')

                @elseif(auth()->user()->role === 'manager')
                        @yield('manager-content')

                @elseif(auth()->user()->role === 'businessManager')
                        @yield('businessManager-content')

                @endif
            </main>
        </div>
    </div>
    
    <script>
        document.getElementById('toggleMenu').addEventListener('click', function() {
            var menu = document.querySelector('.bg-gray-800');
            menu.classList.toggle('hidden');
        });
    </script>
    

</body>

</html>