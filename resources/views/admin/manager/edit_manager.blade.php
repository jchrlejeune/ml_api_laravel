@extends('admin.templateBack')

@section('content')
<div class="container mx-auto w-full lg:w-1/2 m-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Modifier un Conseiller</h1>

    @if(session('success'))
    <div class="bg-green-600 text-white p-4 rounded mb-4">
        {{ session('success') }}
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('admin.manager.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="mb-4">
            <label for="first_name" class="block text-sm font-medium">Prénom :</label>
            <input type="text" id="first_name" name="first_name" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('first_name', $user->information->first_name) }}">
        </div>

        <div class="mb-4">
            <label for="last_name" class="block text-sm font-medium">Nom :</label>
            <input type="text" id="last_name" name="last_name" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('last_name', $user->information->last_name) }}">
        </div>

        <div class="mb-4">
            <label for="phone" class="block text-sm font-medium">Téléphone :</label>
            <input type="text" id="phone" name="phone" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('telephone', $user->information->phone) }}">
        </div>

        <div class="mb-4">
            <label for="email" class="block text-sm font-medium">Email :</label>
            <input type="email" id="email" name="email" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('email', $user->information->email) }}">
        </div>


        <div class="flex justify-center mb-4">
            <button type="submit" class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Mettre à jour Manager
            </button>
        </div>
    </form>
    <div class="flex justify-center my-4">
        <a href="{{ route('admin.manager.manager') }}" class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
            Retour
        </a>
    </div>
</div>
@endsection
