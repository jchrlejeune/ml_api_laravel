@extends('admin.templateBack')

@section('content')
<div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Modifier les Informations</h1>

    @if(session('success'))
    <div class="bg-green-600 text-white p-4 rounded mb-4">
        {{ session('success') }}
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('admin.user.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="mb-4">
            <label for="first_name" class="block text-sm font-medium">Prénom :</label>
            <input type="text" id="first_name" name="first_name" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('first_name', $user->information->first_name) }}">
        </div>

        <div class="mb-4">
            <label for="last_name" class="block text-sm font-medium">Nom :</label>
            <input type="text" id="last_name" name="last_name" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('last_name', $user->information->last_name) }}">
        </div>

        <!-- Date de naissance -->
        <div   class="mb-4">
            <label class="block text-sm font-medium" for="dateOfBirth">date de naissance :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"type="date" id="dateOfBirth" name="dateOfBirth" 
            min="{{ Carbon\Carbon::now()->subYears(25)->format('Y-m-d') }}" max="{{ Carbon\Carbon::now()->subYears(16)->format('Y-m-d') }}"
            value="{{ old('dateOfBirth', $user->information->dateOfBirth) }}" required>
        </div>

        <div class="mb-4">
            <label for="phone" class="block text-sm font-medium">Téléphone :</label>
            <input type="text" id="phone" name="phone" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('telephone', $user->information->phone) }}">
        </div>

        <div class="mb-4">
            <label for="email" class="block text-sm font-medium">Email :</label>
            <input type="email" id="email" name="email" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" value="{{ old('email', $user->information->email) }}">
        </div>

        <div class="mb-4">
            <label for="password">Nouveau mot de passe</label>
            <input 
                type="password" 
                id="password" 
                name="password" 
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="conseiller_id">Conseiller</label>
            <select name="conseiller_id" class="my-2 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
                @foreach ($conseillers as $conseiller)
                    <option value="{{ $conseiller->id }}" @if ($user->conseiller && $user->conseiller->id == $conseiller->id) selected @endif>
                        @if ($conseiller->information)
                            {{ $conseiller->information->first_name }}
                        @else
                            Aucun conseiller
                        @endif
                    </option>
                @endforeach
            </select>
        </div>

        <div class="flex justify-center mb-4">
            <button type="submit" class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Mettre à jour l'utilisateur.
            </button>
        </div>
    </form>

    

    <div class="flex justify-center my-4">
        <a href="{{ route('admin.user.user') }}" class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
            Retour
        </a>
    </div>
</div>
@endsection