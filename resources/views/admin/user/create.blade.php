@extends('admin.templateBack')
@section('content')

<div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    @if (session('success'))
    <div class="bg-green-200 text-green-800 p-3 mb-4">
        {{ session('success') }}
    </div>
    @elseif (session('error'))
    <div class="bg-red-200 text-red-800 p-3 mb-4">
        {{ session('error') }}
    </div>
    @endif

    <form action="{{ route('admin.user.registerCandidat.post') }}" method="POST">

        @csrf
        <!-- Token CSRF pour la sécurité -->

        <!-- Prenom -->
        <div class="mb-4">
            <label class="block text-sm font-medium" for="first_name">Prénom :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" type="text" id="first_name" name="first_name" placeholder="Prénom*" required>
            @error('first_name')
            <div class="text-red-500">{{ $message }}</div>
            @enderror
        </div>


        <!-- Nom de famille -->
        <div class="mb-4">
            <label class="block text-sm font-medium" for="last_name">Nom :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"type="text" id="last_name" name="last_name" placeholder="Nom*" required>
            @error('last_name')
            <div class="text-red-500">{{ $message }}
            </div>
            @enderror
        </div>


        <!-- Téléphone -->
        <div class="mb-4">
            <label class="block text-sm font-medium" for="phone">Téléphone :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" type="tel" id="phone" name="phone" placeholder="Téléphone*" required>
        </div>


        <!-- Email -->
        <div class="mb-4">
            <label class="block text-sm font-medium" for="email">Adresse Mail :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"type="email" id="email" name="email" placeholder="Adresse mail*" required>
        </div>

        <!-- Commune -->
        <div  class="mb-4">
            <label class="block text-sm font-medium" for="city">Commune :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"type="text" id="city" name="city" placeholder="Commune*" required>
        </div>

        <!-- Date de naissance -->
        <div   class="mb-4">
            <label class="block text-sm font-medium" for="dateOfBirth">date de naissance :</label>
            <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"type="date" id="dateOfBirth" name="dateOfBirth" 
            min="{{ Carbon\Carbon::now()->subYears(25)->format('Y-m-d') }}" max="{{ Carbon\Carbon::now()->subYears(16)->format('Y-m-d') }}"
            required>
        </div>

        <!-- Mot de passe -->
        <div  class="mb-4">
        <label class="block text-sm font-medium" for="password">Mot de passe :</label>
        <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" type="password" id="password" name="password" placeholder="Mot de passe*" required>
        @error('password')
        <div class="text-red-500">{{ $message }}</div>
        @enderror
        </div>

        <!-- Confirmation du mot de passe -->
        <div  class="mb-4">
        <label class="block text-sm font-medium"for="password_confirmation">Confirmation du mot de passe :</label>
        <input class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white" type="password" id="password_confirmation" name="password_confirmation"
            placeholder="Confirmation de mot de passe*" required>
        </div>

        <div  class="mb-4">
            @error('email')
            <div class="text-red-500">{{ $message }}</div>
            @enderror
        </div>
        


        <!-- Bouton de soumission -->
        <div class="flex justify-center  mb-4">
            <input class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue w-full" type="submit" value="Envoyer">
        </div>
        
        <div class="flex justify-center mb-4">
    </form>
</div>
@endsection