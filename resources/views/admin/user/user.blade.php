@extends('admin.templateBack')

@section('content')
<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6">Gestion des Jeunes</h1>
    <a href="{{ route('admin.user.create') }}" class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
        Nouveau Jeune
    </a>

    <div class="my-8 flex items-center space-x-4">
        <div class="relative flex-1">
            <input type="text" id="search" placeholder="Rechercher par nom ou email..." class="form-input p-2 w-full pr-10 bg-gray-800 text-white border border-gray-600 rounded focus:outline-none focus:border-blue-500">
            <span class="absolute right-3 top-1/2 transform -translate-y-1/2">
                <i class="fas fa-search text-gray-400"></i>
            </span>
        </div>

    </div>

    <div class="my-8">
        <h2 class="text-xl font-semibold mb-4">Compte(s) activé(s)</h2>
        <div class="overflow-x-auto">
            <table class="min-w-full bg-gray-800 border border-gray-600 rounded">
                <thead>
                    <tr>
                        <th class="text-center bg-gray-700 py-3 px-4">Prénom</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Nom</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Email</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Téléphone</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Conseiller</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Editer</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Compte actif</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usersActifs as $user)
                        <tr>
                            <td class="border-t text-center py-3 px-4">{{ $user->information->first_name }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $user->information->last_name }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $user->information->email }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $user->information->phone }}</td>
                            <td class="border-t text-center py-3 px-4">
                                @if($conseillers->where('jeune_id', $user->id)->isNotEmpty())
                                    {{ $conseillers->where('jeune_id', $user->id)->first()->conseiller->information->first_name }}
                                @else
                                    Aucun conseiller
                                @endif
                            </td>
                            
                            
                            <td class="border-t text-center py-3 px-4">
                                <a href="{{ route('admin.user.edit_user', $user->id) }}" class="text-blue-400 hover:text-blue-200 mr-2">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>

                            <td class="border-t text-center py-3 px-4">
                                <!-- Bouton de cadenas pour mettre à jour le statut actif -->
                                <form action="{{ route('admin.toggleActivation', $user->id) }}" method="POST" class="inline">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="text-white bg-green-500 hover:bg-red-700 py-2 px-4 rounded-full transition duration-300">
                                        {{-- <i class="fas fa-unlock"></i> --}}
                                    </button>
                                </form>
                            </td>
                            <!-- Ajoutez d'autres colonnes si nécessaire -->
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>

    <div class="my-8">
        <h2 class="text-xl font-semibold mb-4">Compte(s) désactivé(s)</h2>
        <div class="overflow-x-auto">
            <table class="min-w-full bg-gray-800 border border-gray-600 rounded">
                <thead>
                    <tr>
                        <th class="text-center bg-gray-700 py-3 px-4">Prénom</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Nom</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Email</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Téléphone</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Conseiller</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Editer</th>
                        <th class="text-center bg-gray-700 py-3 px-4">Compte inactif</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usersInactifs as $userI)
                        <tr>
                            <td class="border-t text-center py-3 px-4">{{ $userI->information->first_name }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $userI->information->last_name }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $userI->information->email }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $userI->information->phone }}</td>
                            <td class="border-t text-center py-3 px-4">
                                @if($conseillers->where('jeune_id', $userI->id)->isNotEmpty())
                                    {{ $conseillers->where('jeune_id', $userI->id)->first()->conseiller->information->first_name }}
                                @else
                                    Aucun conseiller
                                @endif
                            </td>
                            
                            
                            <td class="border-t text-center py-3 px-4">
                                <a href="{{ route('admin.user.edit_user', $userI->id) }}" class="text-blue-400 hover:text-blue-200 mr-2"><i class="fas fa-edit"></i></a>
                            </td>

                            <td class="border-t text-center py-3 px-4">
                                <!-- Bouton de cadenas pour mettre à jour le statut actif -->
                                <form action="{{ route('admin.toggleActivation', $userI->id) }}" method="POST" class="inline">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="text-white bg-red-500 hover:bg-green-700 py-2 px-4 rounded-full transition duration-300">
                                        {{-- <i class="fas fa-lock"></i> --}}
                                    </button>
                                </form>
                            </td>
                            <!-- Ajoutez d'autres colonnes si nécessaire -->
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {

        $('#search').on('input', function() {
            var value = $(this).val().toLowerCase();
            $('tbody tr').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

        // $('#genreFilter').on('change', function() {
        //     var value = $(this).val().toLowerCase();
        //     $('tbody tr').filter(function() {
        //         if (value === '') {
        //             $(this).show();
        //         } else {
        //             $(this).toggle($(this).find('.genre').text().toLowerCase() === value);
        //         }
        //     });
        // });

    });
</script>
@endsection
