@extends('admin.templateBack')
 
@section('content')


<div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Modifier un atelier</h1>
    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif
    <form action="{{ route('admin.atelier.update', $atelier->id) }}" method="POST" enctype="multipart/form-data">
 
        @csrf
        @method('PUT')

        <input type="hidden" name="user_id" value="{{ $atelier->user_id }}">

        <div class="mb-4">
            <label for="title" class="block text-sm font-medium">Titre :</label>
            <input type="text" id="title" name="title" value="{{ old('title', $atelier->title) }}" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
 
        <div class="mb-4">
            <label for="description" class="block text-sm font-medium">Description :</label>
            <textarea id="description" name="description" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ old('description', $atelier->description) }}</textarea>
        </div>
        <div class="mb-4">
            <label for="resume" class="block text-sm font-medium">Résumé du métier :</label>
            <textarea id="resume" name="resume" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ old('resume', $atelier->resume) }}</textarea>
        </div>
        <div class="mb-4">
            <label for="objectif" class="block text-sm font-medium">Objectif :</label>
            <textarea id="objectif" name="objectif" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ old('objectif', $atelier->objectif) }}</textarea>
        </div>
 
        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium text-gray-400">Image :</label>
            <input type="file" id="image_path" value="{{ old('image_path', $atelier->image_path) }}" name="image_path" accept="image/*"  class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
 
        <div class="mb-4">
            <label for="place" class="block text-sm font-medium">Lieu :</label>
            <input type="text" id="place" name="place" value="{{ old('place', $atelier->place) }}" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
 
        <div class="mb-4">
            <label for="date" class="block text-sm font-medium">Date :</label>
            <input type="date" id="date" name="date" min="{{ now()->format('Y-m-d') }}" value="{{ old('date', $atelier->date) }}" 
            class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
       
        <!-- Champ caché pour l'id de l'utilisateur connecté -->
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">


        <div class="flex justify-center mb-4">
            <button type="submit" class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Modifier un Atelier
            </button>
        </div>
    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('admin.atelier.atelier') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>

</div>
 
@endsection