@extends('admin.templateBack')

@section('content')

<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <div class="my-8 flex justify-between items-center">
        <h2 class="text-xl font-semibold mb-4">Atelier</h2>
        <a href="{{ route('admin.atelier.create') }}"
            class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Nouveau
        </a>
        <a href="{{ route('admin.atelier.atelierinactive') }}" class="bg-red-700 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Ateliers Inactifs
        </a>
    </div>
    @if(session('success'))
    <div class="bg-green-600 text-white p-4 rounded mb-4">
        {{ session('success') }}
    </div>
    @endif
    @if(session('error'))
    <div class="bg-red-600 text-white p-4 rounded mb-4">
        {{ session('error') }}
    </div>
    @endif
    <div class="overflow-x-auto">
        <table class="min-w-full bg-gray-800 border border-gray-600 rounded">
            <thead>
                <tr>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Titre</th>
                    {{-- <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Description</th> --}}
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Résumé</th>
                    {{-- <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Objectif</th> --}}
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Image_path</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Place</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Date</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Editer</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Cacher</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activeatelier as $atelier)
                <tr>
                    <td class="py-4 px-6 border-t">{{ $atelier->title }}</td>
                    {{-- <td class="py-4 px-6 border-t">{{  Str::limit($atelier->description, 10, '...')  }}</td> --}}
                    <td class="py-4 px-6 border-t">{{  Str::limit($atelier->resume, 40, '...')  }}</td>
                    {{-- <td class="py-4 px-6 border-t">{{  Str::limit($atelier->objectif, 20, '...')  }}</td> --}}
                    <td class="py-4 px-6 border-t">
                        @if($atelier->image_path)
                            <img src="{{$atelier->image_url}}" alt="{{ $atelier->title }}" class="w-40 h-20 object-cover">
                        @else
                            Pas d'image
                        @endif
                    </td>
                    <td class="py-4 px-6 border-t">{{ $atelier->place }}</td>

                    <td class="py-4 px-6 border-t">{{ $atelier->date }}</td>
                    <td class="py-4 px-6 border-t">
                        <a href="{{ route('admin.atelier.edit', $atelier->id) }}"
                            class="text-blue-400 hover:text-blue-200"><i class="fa-solid fa-pen"></i></a> 
                    </td>
                    <td class="py-4 px-6 border-t">
                        <a href="{{ route('admin.atelier.toggle', $atelier->id) }}"
                            class="text-green-400 hover:text-green-200"><i class="fas fa-lock-open"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    <div class="overflow-x-auto">
</div>
@endsection