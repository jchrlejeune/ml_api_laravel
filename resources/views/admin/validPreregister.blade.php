@extends('admin.templateBack')

@section('content')
    <div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <h1 class="text-2xl text-center font-semibold mb-6">Liste des Pré-inscriptions à validées</h1>
            <ul class="flex flex-col">
                @foreach ($jeunes as $jeune)
                    <li class="bg-gray-700 p-2 mb-2 rounded shadow flex items-center">
                        {{ $jeune->first_name }} {{ $jeune->last_name }} {{ $jeune->email }}
                        <div class="flex space-x-2 ml-auto">
                            <!-- Bouton pour afficher les détails -->
                            <button class="text-blue-400 hover:text-blue-200 show-details-btn">
                                <i class="fa-solid fa-info text-xl"></i>
                            </button>
                            <!-- Bouton pour valider -->
                            <form action="{{ route('admin.validPreregister.post') }}" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $jeune->id }}">
                                
                                <!-- Liste déroulante stylisée -->
                                <select name="conseiller_id" class="bg-gray-700 text-white border border-gray-500 rounded">
                                    <!-- Boucle pour afficher la liste des conseillers disponibles -->
                                    @foreach ($conseillers as $conseiller)
                                        <option value="{{ $conseiller->id }}">{{ $conseiller->first_name }}</option>
                                    @endforeach
                                </select>
            
                                <!-- Bouton pour valider le jeune -->
                                <button type="submit" class="text-green-400 hover:text-green-200">
                                    Valider
                                </button>
                            </form>
                            <!-- Bouton pour supprimer -->
                            <form action="{{ route('admin.validPreregister.upgradejeune', ['id' => $jeune->id]) }}" method="POST">
                                @csrf
                                <button type="submit" class="text-red-400 hover:text-red-200">
                                    <i class="fa-sharp fa-solid fa-trash text-xl"></i>
                                </button>
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>

        
        <br>
        <h1 class="text-2xl text-center font-semibold mb-6">Liste des Pré-inscriptions Refusées</h1>
        
               
        <ul class="flex flex-col">
            @foreach ($refusjeune as $refus)
                <li class="bg-gray-700 p-2 mb-2 rounded shadow flex items-center">  
                    {{ $refus->first_name }} {{ $refus->last_name }} {{ $refus->email }}
                    <div class="flex space-x-2 ml-auto">
                        <!-- Bouton pour afficher les détails -->
                        <button class="text-blue-400 hover:text-blue-200 show-details-btn">
                            <i class="fa-solid fa-info text-xl"></i>
                        </button>
                        
                        <!-- Bouton pour supprimer -->
                        <form action="{{ route('admin.validPreregister.deletejeune', ['id' => $refus->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="text-red-400 hover:text-red-200">
                                <i class="fa-sharp fa-solid fa-trash text-xl"></i>
                            </button>
                        </form>
                    </div>
                </li>
            @endforeach
        </ul>
       
    </div>


    
    
    
    
    
    
    
    
    
    
    
    <div id="defaultModal" tabindex="-1" aria-hidden="true"
        class="modal fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
        <div class="relative w-full max-w-2xl max-h-full">
            <!-- Modal content -->
            <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                <!-- Modal header -->
                <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">

                    <div id="userDetailsContent">

                    </div>
                    <button type="button" id="closeModalBtn"
                        class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                        data-modal-hide="defaultModal">
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                        </svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>

            </div>
        </div>
    </div>






    <script>
        $(document).ready(function() {
            // Lorsqu'un bouton "Info" est cliqué
            $('.show-details-btn').click(function() {
                // Récupérez l'ID de la préinscription à partir de l'attribut data-preregistration-id
                var preregistrationId = $(this).data('preregistration-id');

                var propertyLabels = {
                    'first_name': 'Prénom',
                    'last_name': 'Nom de Famille',
                    'gender': 'Genre',
                    'address': 'Adresse',
                    'phone': 'Téléphone',
                    'email': 'Email',
                    'situation': 'Situation',
                    'housing': 'Membre dans la famille',
                    'income': 'Revenu',
                    'education_level': 'Niveau d\'étude',
                    'cv_path': 'CV'
                };


                // Faites une requête AJAX pour récupérer les détails de la préinscription à partir du serveur
                $.ajax({
                    url: '/admin/validPreregister/user_details',
                    type: 'GET',
                    data: {
                        preregistration_id: preregistrationId
                    },
                    success: function(response) {
                        // Mettez à jour le contenu de la popup avec les détails de la préinscription
                        var userDetailsHtml = '';

                        // Itérez sur le tableau associatif et générez le contenu HTML
                        for (var property in propertyLabels) {
                            if (response[property] !== null) {
                                userDetailsHtml +=
                                    `<p><strong>${propertyLabels[property]}:</strong> ${response[property]}</p>`;
                            }
                        }

                        $('#userDetailsContent').html(userDetailsHtml);

                        // Affichez la popup
                        $('.modal').removeClass('hidden');
                    }
                });
            });

            // Lorsqu'un bouton "Fermer" dans la popup est cliqué
            $('#closeModalBtn').click(function() {
                // Masquez la popup
                $('.modal').addClass('hidden');
            });
        });
    </script>
@endsection
