@extends('admin.templateBack')

@section('content')

<div class="container mx-auto w-full lg:w-1/2 mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lgg">
    <h2 class="text-2xl font-semibold mb-6 text-center text-white">Photo de groupe</h2>
    
    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif
    
    <form action="{{ route('admin.photogroupe.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium text-gray-400">Image :</label>
            <input type="file" id="photo_groupe" name="image_path" accept="image/jpeg" required
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="flex justify-center mb-4">
            <button type="submit"
                class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue w-full">
                Ajouter photo
            </button>
        </div>
    </form>

    <div class="flex justify-center mb-4">
        <a href="{{ route('admin.photogroupe.photogroupe') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>

@endsection