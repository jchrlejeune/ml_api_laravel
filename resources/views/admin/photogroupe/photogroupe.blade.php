@extends('admin.templateBack')

@section('content')
<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6">Photo de groupe</h1>

    <div class="mb-6">
        <a href="{{ route('admin.photogroupe.create') }}"
            class="bg-gray-600 hover:bg-gray-700 text-white font-semibold py-2 px-4 rounded-full inline-block transition duration-300">
            Ajouter une photo
        </a>
    </div>

    <div class="grid grid-cols-1 sm:grid-cols-1 lg:grid-cols-2">
        @foreach($photogroupes as $photogroupe)
        <div class="bg-gray-900 hover:bg-gray-700 p-6 border rounded-lg shadow-lg transition duration-300">
            <h3 class="text-xl font-semibold mb-2 text-white">{{ $photogroupe->title }}</h3>
            <div class="flex w-full items-center mb-4">
                <img src="{{ $photogroupe->image_url }}" alt="photo_de_groupe"
                    class="mt-1 p-2 w-100 h-64 object-cover rounded-md">

                <p class="text-gray-400">{{ $photogroupe->description }}</p>
            </div>
            <div class="flex items-center">
                <a href="{{ route('admin.photogroupe.edit', $photogroupe->id) }}"
                    class="text-blue-500 hover:underline mr-4">
                    <i class="fas fa-edit mr-1"></i>Editer
                </a>
                <form action="{{ route('admin.photogroupe.destroy', $photogroupe->id) }}" method="POST"
                    class="inline form-delete">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="text-red-500 hover:underline">
                        <i class="fas fa-trash-alt mr-1"></i>Supprimer
                    </button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</div>



@endsection
