@if (session('success'))
<div class="bg-green-200 text-green-800 p-3 mb-4">
    {{ session('success') }}
</div>
@elseif (session('error'))
<div class="bg-red-200 text-red-800 p-3 mb-4">
    {{ session('error') }}
</div>
@endif

<form action="{{ route('admin.registerEntreprise.post') }}" method="POST">

    @csrf
    <!-- Token CSRF pour la sécurité -->

    <!-- Nom de l'entreprise -->
    <label for="company_name">Nom de l'entreprise :</label>
    <input type="text" id="company_name" name="company_name" placeholder="Entreprise*" required>
    @error('company_name')
    <div class="text-red-500">{{ $message }}</div>
    @enderror
    <br><br>

    <!-- SIRET -->
    <label for="siret">Numéro SIRET :</label>
    <input type="text" id="siret" name="siret" placeholder="Numéro SIRET*" required>
    @error('siret')
    <div class="text-red-500">{{ $message }}</div>
    @enderror
    <br>
    <br>

    <!-- Nom du responsable -->
    <label for="responsible_name">Interlocuteur :</label>
    <input type="text" id="responsible_name" name="responsible_name" placeholder="Interlocuteur*" required>
    @error('responsible_name')
    <div class="text-red-500">{{ $message }}</div>
    @enderror
    <br><br>

    <!-- Téléphone -->
    <label for="company_phone">Téléphone :</label>
    <input type="tel" id="company_phone" name="company_phone" placeholder="Téléphone*" required>
    @error('company_phone')
    <div class="text-red-500">{{ $message }}</div>
    @enderror
    <br>
    <br>

    <!-- Email -->
    <label for="company_email">Email :</label>
    <input type="email" id="company_email" name="company_email" placeholder="Adresse mail*" required>
    @error('company_email')
    <div class="text-red-500">{{ $message }}</div>
    @enderror
    <br>
    <br>

    <!-- Mot de passe -->
    <label for="password">Mot de passe :</label>
    <input type="password" id="password" name="password" placeholder="Mot de passe*" required>
    @error('password')
    <div class="text-red-500">{{ $message }}</div>
    @enderror
    <br><br>

    <!-- Confirmation du mot de passe -->
    <label for="password_confirmation">Confirmation du mot de passe :</label>
    <input type="password" id="password_confirmation" name="password_confirmation"
        placeholder="Confirmation de mot de passe*" required>
    <br><br>

    <!-- Accepter -->
    <!--
    <p></p>
    <input type="checkbox" id="confirmationInscriptionEntreprise" name="confirmationInscriptionEntreprise" />
    <label for="confirmationInscriptionEntreprise">J'accepte d'être contacté.e par la Mission Locale des Mureaux et j'accepte que les informations fournies alimentent mon dossier d'inscription.</label>
    <p></p>
    -->

    <!-- Bouton de soumission -->
    <input type="submit" value="Envoyer">
</form>