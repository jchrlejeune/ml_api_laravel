@extends('admin.templateBack')

@section('content')

<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
  

    <div class="my-8 flex justify-between items-center">
        <h2 class="text-xl font-semibold mb-4">Chargé Entreprise Actif</h2>
        <a href="{{ route('admin.businessManager.create') }}" class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Nouveau
        </a>
        <a href="{{ route('admin.businessManager.businessManagerInactive') }}" class="bg-red-700 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-gray">
            Chargé Entreprise Inactif
        </a>
    </div>
    @if(session('success'))
    <div class="bg-green-600 text-white p-4 rounded mb-4">
        {{ session('success') }}
    </div>
@endif
    <div class="overflow-x-auto">
        <table  class="min-w-full bg-gray-800 border border-gray-600 rounded">
            <thead>
                <tr>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Prénom</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Nom</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Téléphone</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Email</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Editer</th>
                    <th class="py-3 px-6 text-left bg-gray-700 font-semibold">Comptes activés</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activeBusinessManagers as $businessmanager)
                <tr>
                    <td class="py-4 px-6 border-t">{{ $businessmanager->information->first_name }}</td>
                    <td class="py-4 px-6 border-t">{{ $businessmanager->information->last_name }}</td>
                    <td class="py-4 px-6 border-t">{{ $businessmanager->information->phone }}</td>
                    <td class="py-4 px-6 border-t">{{ $businessmanager->information->email }}</td>
                    <td  class="py-4 px-6 border-t">
                        <a href="{{ route('admin.businessManager.edit_manager', $businessmanager->id) }}" class="text-blue-400 hover:text-blue-200 mr-2">
                            <i class="fas fa-edit"></i>
                        </a>
                    </td>
                    <td class="py-4 px-6 border-t">
                        
                        <a href="{{ route('admin.businessManager.toggle', $businessmanager->id) }}">
                            <button type="submit" class="text-white bg-green-500 hover:bg-red-700 py-2 px-4 rounded-full transition duration-300"></button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
