@extends('admin.templateBack')
@section('content')
    <div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        @if (session('success'))
            <div class="bg-green-200 text-green-800 p-3 mb-4">
                {{ session('success') }}
            </div>
        @elseif (session('error'))
            <div class="bg-red-200 text-red-800 p-3 mb-4">
                {{ session('error') }}
            </div>
        @endif

        <form action="{{ route('admin.company.update', ['id' => $user->id]) }}" method="POST">
            @csrf
            @method('PUT') <!-- Méthode PUT pour la modification -->
            <!-- Token CSRF pour la sécurité -->

            <!-- Nom de l'entreprise -->
            <label class="block text-sm font-medium" for="company_name">Nom de l'entreprise :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="text" id="company_name" name="company_name" placeholder="Entreprise*" value="{{ old('company_name', $user->information->company_name) }}">
            @error('company_name')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Numéro SIRET -->
            <label class="block text-sm font-medium" for="siret">Numéro SIRET :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="text" id="siret" name="siret" placeholder="Numéro SIRET*" value="{{ old('siret', $user->information->siret) }}">
            @error('siret')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Interlocuteur -->
            <label class="block text-sm font-medium" for="responsible_name">Interlocuteur :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="text" id="responsible_name" name="responsible_name" placeholder="Interlocuteur*" value="{{ old('responsible_name', $user->information->responsible_name) }}">
            @error('responsible_name')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Téléphone de l'entreprise -->
            <label class="block text-sm font-medium" for="company_phone">Téléphone :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="tel" id="company_phone" name="company_phone" placeholder="Téléphone*" value="{{ old('company_phone', $user->information->company_phone) }}">
            @error('company_phone')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Email de l'entreprise -->
            <label class="block text-sm font-medium" for="company_email">Email :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="email" id="company_email" name="company_email" placeholder="Adresse mail*" value="{{ old('email', $user->information->email) }}">
            @error('company_email')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Mot de passe -->
            <div class="mb-4">
                <label for="password">Nouveau mot de passe</label>
                <input 
                    type="password" 
                    id="password" 
                    name="password" 
                    class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
            </div>

            <!-- Bouton de soumission -->
            <div class="flex justify-center mb-4">
                <button type="submit" class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                    Mettre à jour l'utilisateur.
                </button>
            </div>

            <div class="flex justify-center my-4">
                <a href="{{ route('admin.company.company') }}" class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                    Retour
                </a>
            </div>
        </form>
    </div>
@endsection
