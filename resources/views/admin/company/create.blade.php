@extends('admin.templateBack')
@section('content')
    <div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        @if (session('success'))
            <div class="bg-green-200 text-green-800 p-3 mb-4">
                {{ session('success') }}
            </div>
        @elseif (session('error'))
            <div class="bg-red-200 text-red-800 p-3 mb-4">
                {{ session('error') }}
            </div>
        @endif

        <form action="{{ route('admin.registerEntreprise.post') }}" method="POST">

            @csrf
            <!-- Token CSRF pour la sécurité -->

            <!-- Nom de l'entreprise -->
            <label class="block text-sm font-medium" for="company_name">Nom de l'entreprise :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="text" id="company_name" name="company_name" placeholder="Entreprise*" required>
            @error('company_name')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Numéro SIRET -->
            <label class="block text-sm font-medium" for="siret">Numéro SIRET :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="text" id="siret" name="siret" placeholder="Numéro SIRET*" required>
            @error('siret')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Interlocuteur -->
            <label class="block text-sm font-medium" for="responsible_name">Interlocuteur :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="text" id="responsible_name" name="responsible_name" placeholder="Interlocuteur*" required>
            @error('responsible_name')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Téléphone de l'entreprise -->
            <label class="block text-sm font-medium" for="company_phone">Téléphone :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="tel" id="company_phone" name="company_phone" placeholder="Téléphone*" required>
            @error('company_phone')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Email de l'entreprise -->
            <label class="block text-sm font-medium" for="company_email">Email :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="email" id="company_email" name="company_email" placeholder="Adresse mail*" required>
            @error('company_email')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Mot de passe -->
            <label class="block text-sm font-medium" for="password">Mot de passe :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="password" id="password" name="password" placeholder="Mot de passe*" required>
            @error('password')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Confirmation du mot de passe -->
            <label class="block text-sm font-medium" for="password_confirmation">Confirmation du mot de passe :</label>
            <input
                class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"
                type="password" id="password_confirmation" name="password_confirmation"
                placeholder="Confirmation de mot de passe*" required>
            @error('password_confirmation')
                <div class="text-red-500">{{ $message }}</div>
            @enderror
            <br><br>

            <!-- Bouton de soumission -->
            <div class="flex justify-center  mb-4">
                <input
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue w-full"
                    type="submit" value="Envoyer">
            </div>

            <div class="flex justify-center mb-4">
        </form>
    </div>
@endsection
