<body>
    @extends('header')
    @section('body')
        <section class="h-screen">
            <div class="image-container">
                <img src="Images/barre_colore.png"  style="width: 100%; height:20px">
            </div>
            <div class="logo-inscription-block">
                <img src="Images/logo-inscription.svg" class="logo-inscription" >
            </div>
            <article class="formulaire-inscription">

                <div class="containe">
                    <form action="{{ route('auth.jeune_login.post') }}" method="POST">
                        @csrf
                        <h3 class="titre-inscription">Espace Administration</h3>
                        @if (session('error'))
                            <div style="color: red;">
                                {{ session('error') }}
                            </div>
                        @endif
                        <input type="email" name="email" placeholder="Adresse e-mail*" required
                            class="input-inscription">
                        <input type="password" name="password" placeholder="Mot de passe*" required
                            class="input-inscription">
                        <button type="submit" class="input-inscription-button">SE CONNECTER</button>
                    </form>
                </div>
            </article>
        </section>
    @endsection
</body>
</html>
