<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Site de la Mission Locale des Mureaux" />
    <meta name="author" content="Mission Locale" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Source+Serif+Pro:ital,wght@1,300&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <title>Mission Locale</title>
</head>

<body>
    <header>
        <section id="header2">
            <a href="{{ route('home') }}">
                <img src="/Images/logo1.png" alt="logo" id="logo">
            </a>
        </section>
    </header>

    @yield('body')
