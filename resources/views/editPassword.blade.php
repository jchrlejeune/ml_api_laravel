<div class="container">
    <h1 class="text-3xl font-semibold mb-6">Modifier le Mot de Passe</h1>
    <form action="{{ route('updatePassword') }}" method="post">
        @csrf
        @method('PUT')

        <div class="mb-4">
            <label for="current_password" class="block text-sm font-medium text-gray-600">Mot de Passe Actuel</label>
            <input type="password" name="current_password" id="current_password" class="form-input mt-1 p-2 w-full" required>
        </div>

        <div class="mb-4">
            <label for="new_password" class="block text-sm font-medium text-gray-600">Nouveau Mot de Passe</label>
            <input type="password" name="new_password" id="new_password" class="form-input mt-1 p-2 w-full" required>
        </div>

        <div class="mb-4">
            <label for="new_password_confirmation" class="block text-sm font-medium text-gray-600">Confirmer le Nouveau Mot de Passe</label>
            <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-input mt-1 p-2 w-full" required>
        </div>

        <div class="mt-4">
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Enregistrer le Nouveau Mot de Passe
            </button>
        </div>
    </form>
</div>