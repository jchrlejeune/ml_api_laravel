    @extends("header")
    @section('body')

    <section class="image-container">
      <a href="{{ route('home') }}">
        <img src="{{ asset('Images/ROUGE.jpg') }}"  style="width: 100%;">
        <div class="image-title">
          <h1 class="text-center">Une erreur inconnue est survenue</h1>
        </div>
      </a>
    </section>

    <section  class="image-container">
      <article class="p-4">
          <a href="{{ route('home') }}">
            <p class="text-center w-full">
              Code d'erreur : {{ $exception->getCode() }}<br>
              Message d'erreur : {{ $exception->getMessage() }}<br>
              Veuillez Réessayer plus tard.
            </p>
          </a>
      </article>
    </section>

    @endsection
  </body>
  
