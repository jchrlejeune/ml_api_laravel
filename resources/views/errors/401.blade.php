    @extends("header")
    @section('body')
  
    <section class="image-container">
      <a href="{{ route('home') }}">
        <img src="{{ asset('Images/ROUGE.jpg') }}" style="width: 100%;">
        <div class="image-title">
          <h1 class="text-center">Erreur 401</h1>
        </div>
      </a>
    </section>

    <section  class="image-container">
      <article class="p-4">
          <a href="{{ route('home') }}">
            <p class="text-center">Informations d'identification invalides.</p>
            <p class="text-center">Veuillez contacter l'administrateur du site.</p>
          </a>
      </article>
    </section>
  
    @endsection
  </body>
  
  </html>