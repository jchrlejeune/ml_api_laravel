    @extends("header")
    @section('body')

    <section class="image-container">
      <a href="{{ route('home') }}">
        <img src="{{ asset('Images/ROUGE.jpg') }}"  style="width: 100%;">
        <div class="image-title">
          <h1 class="text-center">Erreur de connexion à la base de données</h1>
        </div>
      </a>
    </section>
    
    <section  class="image-container">
      <article class="p-4">
          <a href="{{ route('home') }}">
            <p class="text-center">La base de données est actuellement inaccessible.<br>Veuillez réessayer plus tard.</p>
          </a>
      </article>
    </section>
  
  @endsection
  <script src="{{ asset('js/script.js') }}"></script>
  </body>
  
  </html>