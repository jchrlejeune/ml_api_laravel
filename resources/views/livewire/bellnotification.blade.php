<!-- resources/views/livewire/bellnotification.blade.php -->
<div>
    <a href="{{ route('admin.showNotifications') }}" class="text-gray-500 hover:text-gray-700 transition duration-300">
        <!-- Icône de cloche pour les notifications -->
        <span class="relative inline-block">
            <!-- Point rouge ou nombre de notifications -->
            <span 
                @class([
                    'bg-red-500' => $toto > 0,
                    'text-white' => $toto > 0,
                    'text-gray-500' => $toto === 0,
                    'rounded-full' => true,
                ])>
                {{ $toto }}
            </span>
        </span>
    </a>
    <button wire:click="increment">+</button>
    <button wire:click="resettoto">reset</button>
</div>
