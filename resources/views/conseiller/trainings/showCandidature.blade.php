@extends('admin.templateBack')

@section('manager-content')
    <div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <div class="my-8 flex justify-between items-center">
            <h2 class="text-xl font-semibold mb-4">Détails de la formation "{{ $formation->title }}"</h2>
        </div>
        <ul class="divide-y divide-gray-700">
            @foreach ($candidaturesEnAttente as $candidature)
                <li class="py-4">
                    <div class="flex justify-between items-center">
                        <div class="flex flex-col">
                            <div class="font-semibold">{{ $candidature->user->information->last_name }} {{ $candidature->user->information->first_name }}</div>
                            <div class="text-gray-400 text-sm">{{ $candidature->status }}</div>
                        </div>
                        <div class="flex space-x-4">
                            <form action="{{ route('formations.validerCandidature', $candidature->id) }}" method="POST">
                                @csrf
                                <button type="submit" class="text-green-500 hover:text-green-300 focus:outline-none">
                                    <i class="fa-solid fa-check-circle"></i>
                                </button>
                            </form>
                            <form action="{{ route('formations.refuserCandidature', $candidature->id) }}" method="POST">
                                @csrf
                                <button type="submit" class="text-red-500 hover:text-red-300 focus:outline-none">
                                    <i class="fa-solid fa-times-circle"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
