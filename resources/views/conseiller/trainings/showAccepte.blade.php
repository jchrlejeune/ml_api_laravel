@extends('admin.templateBack')

@section('manager-content')
    <div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <h1 class="text-3xl font-semibold mb-6 text-center">Personnes Acceptées pour {{ $formation->title }}</h1>

        <div class="grid grid-cols-2 gap-4">
            @foreach($usersAcceptes as $user)
                <div class="bg-gray-800 p-4 rounded-lg shadow-md">
                    <h2 class="text-xl font-semibold mb-2">Jeune : {{ $user->identifiant }}</h2>
                    @if($user->conseillerJeunes)
                        @foreach($user->conseillerJeunes as $conseillerJeune)
                            <p class="text-gray-400 mb-4">
                                Conseiller : {{ $conseillerJeune->conseiller->information->last_name ?? 'N/A' }}
                                {{ $conseillerJeune->conseiller->information->first_name ?? 'N/A' }}
                            </p>
                        @endforeach
                    @else
                        <p class="text-gray-400">Aucun conseiller trouvé.</p>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endsection