@extends('admin.templateBack')

@section('manager-content')

    <div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <h1 class="text-3xl font-semibold mb-6 text-center">Envoyer un document à {{ $jeune->information->last_name }}</h1>

        @if(session('success'))
            <div class="bg-green-600 text-white p-4 rounded mb-4">
                {{ session('success') }}
            </div>
        @endif

        <form action="{{ route('conseiller.sendDoc.store', $jeune->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            
            <input type="hidden" name="jeune_id" value="{{ $jeune->id }}">
            <!-- Ajoutez les champs du formulaire ici -->

            <div class="mb-4">
                <label for="title" class="block text-sm font-medium text-gray-400">Titre du document:</label>
                <input type="text" id="title" name="title" class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
            </div>

            <div class="mb-4">
                <label for="document" class="block text-sm font-medium text-gray-400">Document pdf (Max: 4Mo):</label>
                <input type="file" id="document" name="document" accept="application/pdf" class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
            </div>

            <div class="flex justify-center">
                <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                    Envoyer
                </button>
            </div>
        </form>
    </div>

@endsection