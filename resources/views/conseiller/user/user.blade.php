@extends('admin.templateBack')

@section('manager-content')
<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6">Gestion des Jeunes</h1>

    <div class="my-8 flex items-center space-x-4">
        <div class="relative flex-1">
            <input type="text" id="search" placeholder="Rechercher par nom ou email..." class="form-input p-2 w-full pr-10 bg-gray-800 text-white border border-gray-600 rounded focus:outline-none focus:border-blue-500">
            <span class="absolute right-3 top-1/2 transform -translate-y-1/2">
                <i class="fas fa-search text-gray-400"></i>
            </span>
        </div>

    </div>

    <div class="my-8">
        <h2 class="text-xl font-semibold mb-4">Jeunes suivis</h2>
        <div class="overflow-x-auto">
            <table class="min-w-full bg-gray-800 border border-gray-600 rounded">
                <thead>
                    <tr>
                        
                        <th class="text-center  bg-gray-700 py-3 px-4">Prénom</th>
                        <th class="text-center  bg-gray-700 py-3 px-4">Nom</th>
                        <th class="text-center  bg-gray-700 py-3 px-4">Email</th>
                        <th class="text-center  bg-gray-700 py-3 px-4">Téléphone</th>
                        <th class="text-center  bg-gray-700 py-3 px-4">Conseiller</th>
                        <th class="text-center  bg-gray-700 py-3 px-4">Editer</th>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Générer PDF</th>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Rendez-Vous</th>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Envoyer un Document</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($conseiller->jeunes as $jeune)
                        <tr>
                            <td class="border-t text-center py-3 px-4">{{ $jeune->information->first_name }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $jeune->information->last_name }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $jeune->information->email }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $jeune->information->phone }}</td>
                            <td class="border-t text-center py-3 px-4">{{ $conseiller->information->first_name }}</td>
                            <td class="border-t text-center py-3 px-4">
                                <a href="{{ route('conseiller.user.edit_user', $jeune->id) }}" class="text-2xl text-blue-400 hover:text-blue-200 mr-2">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>
                            <td class="border-t text-center py-3 px-4">
                                <a href="{{ route('generatePDF', $jeune->id) }}" class="text-2xl py-4 px-6 text-center">
                                    <i class="fa-regular fa-file-pdf" ></i>
                                </a>
                            </td>
                            <td class="border-t text-center py-3 px-4">
                                <a href="{{ route('conseiller.appointments.create', $jeune->id) }}" class="text-2xl py-4 px-6 text-center">
                                    <i class="fa-regular fa-calendar-plus"></i>
                                </a>
                            </td>
                            <td  class="border-t text-center py-3 px-4">
                                <a href="{{ route('conseiller.sendDoc.send', $jeune->id) }}" class="text-2xl py-4 px-6 text-center">
                                    <i class="fa-regular fa-share-from-square"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {

        $('#search').on('input', function() {
            var value = $(this).val().toLowerCase();
            $('tbody tr').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

    });
</script>
@endsection
