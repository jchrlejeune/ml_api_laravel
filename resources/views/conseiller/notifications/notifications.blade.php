@extends('admin.templateBack')

@section('manager-content')
<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6">Liste des notifications</h1>
    
    <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
        @foreach($notifications as $notification)
            <div class="bg-gray-800  hover:bg-gray-700 hover:text-white p-6 mb-4 border rounded-lg shadow-lg transition duration-300 transform hover:scale-105">
                @php
                    $data = json_decode($notification->data, true);
                @endphp

                @php
                    $jeune = \App\Models\User::find($data['jeune_id']);
                    $nomJeune = $jeune ? $jeune->information->first_name . ' ' . $jeune->information->last_name : 'Utilisateur inconnu';
                    $uuid = $notification->id;
                @endphp

                <h3 class="text-xl font-semibold mb-2 text-white">{{ $nomJeune }}</h3>
                
                <div class="flex items-center mb-4">
    <p class="text-gray-400">
        @if (isset($data['choix']))
            @if ($data['choix'] === 'Avoir un RDV')
                Avoir un rendez-vous
            @elseif ($data['choix'] === 'Chercher une formation')
                Chercher une formation
            @elseif ($data['choix'] === 'Trouver un emploi')
                Trouver un emploi
            @else
                {{ $data['choix'] }}
            @endif
        @endif

        @if (isset($data['offre_id']))
            {{-- Ajoutez la logique pour gérer offre_id ici --}}
            @php
                $jobOffer = \App\Models\Joboffer::find($data['offre_id']);
            @endphp

            @if ($jobOffer)
                postule pour: {{ $jobOffer->title }}
            @else
                Offre d'emploi introuvable
            @endif
        @endif
    </p>
</div>
                
                <p class="text-gray-500 mb-2"><i class="fas fa-clock mr-2"></i>{{ $notification->created_at->diffForHumans() }}</p>
                
                <div class="flex items-center">
                    <form action="{{ route('conseiller.notification.delete', $notification->id) }}" method="POST" class="inline form-delete">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="uuid" value="{{ $notification->id }}">
                        <button type="submit" class="text-red-500 hover:underline">
                            Supprimer
                        </button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
{{-- @section('manager-content')
    <div class="mx-auto mt-8 bg-gray-900 text-white p-8 border rounded-lg shadow-lg">
        <h1 class="text-3xl font-semibold mb-6 text-center">Formations</h1>

        <div class="grid grid-cols-2 gap-4">
            @foreach ($formations as $formation)
                <div class="bg-gray-800 p-4 rounded-lg shadow-md">
                    <h2 class="text-xl font-semibold mb-2">Titre:{{ $formation->title }}</h2>
                    <p class="text-gray-400 mb-4">Job:{{ $formation->job }}</p>
                    <p class="text-gray-400 mb-4">{{ $formation->description }}</p>
                    <a href="{{ route('conseiller.trainings.showCandidature', $formation->id) }}"
                        class="text-blue-500 hover:underline">{{ $candidaturesParFormation[$formation->id]['en_attente'] }}
                        candidatures</a>


                    <a href=""
                        class="text-green-500 hover:underline">Acceptées:
                        {{ $candidaturesParFormation[$formation->id]['accepte'] }}</a>
                </div>
            @endforeach
        </div>
    </div>
@endsection --}}

