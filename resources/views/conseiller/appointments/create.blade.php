@extends('admin.templateBack')

@section('manager-content')

    <div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <h1 class="text-3xl font-semibold mb-6 text-center">Créer un Rendez-vous avec {{ $jeune->information->last_name }}</h1>

        @if (session('success'))
        <div class="bg-green-200 text-green-800 p-3 mb-4">
            {{ session('success') }}
        </div>
        @elseif (session('error'))
        <div class="bg-red-200 text-red-800 p-3 mb-4">
            {{ session('error') }}
        </div>
        @endif

        <form action="{{ route('conseiller.appointments.store') }}" method="POST">
            @csrf
            
            <input type="hidden" name="jeune_id" value="{{ $jeune->id }}">
            <!-- Ajoutez les champs du formulaire ici -->
            <div class="mb-4">
                <label for="date" class="block text-sm font-medium">Date :</label>
                <input required type="date" id="date" name="date" min="{{ now()->format('Y-m-d') }}" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
            </div>

            <div class="mb-4">
                <label for="heure" class="block text-sm font-medium">Heure :</label>
                <input required type="time" id="heure" name="heure" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
            </div>

            <div class="mb-4">
                <label for="description" class="block text-sm font-medium">Description :</label>
                <textarea id="description" name="description" rows="4" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
            </div>

            <div class="flex justify-center">
                
                <button type="submit" class="my-8 w-64 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                    Créer Rendez-vous
                </button>
            </div>
        </form>
        <div class="flex justify-center mb-4">
            <a href="{{ route('conseiller.appointments.calendar') }}"
                class=" w-64 bg-blue-500 hover:bg-blue-700 text-white text-center font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
        </div>
    </div>

@endsection