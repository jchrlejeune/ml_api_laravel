@extends('admin.templateBack')

@section('manager-content')
    <style>
        .info-bulle {
            position: absolute;
            background-color: #f1f1f1;
            padding: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        @media (max-width: 1024px) {
            /* Styles pour les écrans de largeur comprise entre 641px et 1024px */
            .calendar-large {
                display: none; /* Masquer la vue écran large */
            }
        }

        @media (max-width: 640px) {
            /* Styles pour les écrans de largeur inférieure ou égale à 640px */
            .calendar-small {
                display: none; /* Masquer la vue mobile */
            }
        }
    </style>
    <div class="container min-w-full lg:w-1/2  mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <h1 class="text-3xl font-semibold mb-6 text-center text-white">Rendez-vous</h1>

        <div class="flex justify-between items-center mb-4">
            <div class="flex items-center space-x-4">
                <button class="bg-blue-500 text-white px-2 py-2 rounded-md" onclick="previousMonth()">&lt;</button>
                <div id="currentMonth" class="text-xl font-semibold text-white"></div>
                <button class="bg-blue-500 text-white px-2 py-2 rounded-md" onclick="nextMonth()">&gt;</button>
            </div>
            <div class="flex items-center space-x-4">
                <a href="{{ route('conseiller.user.user') }}">
                    <button class="bg-blue-500 text-white px-2 py-2 rounded-md">Ajouter un rendez vous</button>
                </a>
            </div>
            
        </div>
        
        <div class="overflow-x-auto" id="calendar-container">
            <table class="w-full bg-gray-800 border border-gray-600 rounded calendar" id="calendar-table">
                <thead class="hidden sm:table-header-group"> <!-- Masquer sur les petits écrans -->
                    <tr>
                        <th class="p-3 text-center border-none bg-red-900 font-semibold">Dimanche</th>
                        <th class="p-3 text-center border-none bg-gray-700 font-semibold">Lundi</th>
                        <th class="p-3 text-center border-none bg-gray-700 font-semibold">Mardi</th>
                        <th class="p-3 text-center border-none bg-gray-700 font-semibold">Mercredi</th>
                        <th class="p-3 text-center border-none bg-gray-700 font-semibold">Jeudi</th>
                        <th class="p-3 text-center border-none bg-gray-700 font-semibold">Vendredi</th>
                        <th class="p-3 text-center border-none bg-gray-700 font-semibold">Samedi</th>
                    </tr>
                </thead>
                <tbody id="calendar-body">
                    <!-- Le contenu du calendrier sera ajouté ici dynamiquement -->
                </tbody>
            </table>
        </div>
        
        <div class="overflow-x-auto" id="list-container">
            <table class="lg:hidden w-full bg-gray-800 border border-gray-600 rounded calendar">
                <tbody id="calendar-list">
                    <!-- Le contenu du tableau sera ajouté ici dynamiquement -->
                </tbody>
            </table>
        </div>
        
        
        
    @auth
        <script>
            const userToken = "{{ auth()->user()->createToken('web')->plainTextToken }}";
        </script>
    @endauth
    <script>
        let currentMonth = "{{ $currentMonth->format('Y-m-d') }}"; // Convertir la date PHP en chaîne JS
        currentMonth = new Date(currentMonth);

        function annulerRendezVous(rendezvousId) {
            const deleteButton = $(this); // Ajoutez cette ligne pour obtenir une référence au bouton
            deleteButton.prop('disabled', true);

            $.ajax({
                url: `/conseiller/rendez-vous/${rendezvousId}/cancel`,
                type: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${userToken}`,
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    console.log(response);
                    // Recharger la page après annulation réussie
                    location.reload();
                },
                error: function(error) {
                    console.error('Erreur lors de l\'annulation du rendez-vous:', error);
                },
                complete: function() {
                    deleteButton.prop('disabled', false);
                }
            });
        }

        function afficherInfoBulle(event, rendezvous) {
            console.log("infobulle")
            const infoBulle = document.createElement('div');
            infoBulle.classList.add('info-bulle');

            const details = document.createElement('div');
            details.innerHTML = `
            <p>${rendezvous.jeune.information.first_name} ${rendezvous.jeune.information.last_name}</p>
            <p>Date: ${moment(rendezvous.date, "HH:mm:ss").format("L")}</p>
            <p>Heure: ${moment(rendezvous.heure, "HH:mm:ss").format("HH:mm")}</p>
            ${rendezvous.description ? `<p>Description: ${rendezvous.description}</p>` : ''}
        `;

            infoBulle.appendChild(details);
            infoBulle.style.top = `${event.clientY}px`;
            infoBulle.style.left = `${event.clientX}px`;
            document.body.appendChild(infoBulle);
        }
        // Fonction pour générer le calendrier pour le mois en cours
        function generateCalendar() {
            document.querySelector('#currentMonth').textContent = currentMonth.toLocaleString('fr-FR', {
                month: 'long',
                year: 'numeric'
            });
            const firstDayOfMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth(), 1).getDay();
            const daysInMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1, 0).getDate();
            const calendarBody = document.querySelector('#calendar-body');
            calendarBody.innerHTML = ''; // Supprime le contenu précédent du corps du calendrier

            let dayCounter = 1;

            for (let i = 0; i < 6; i++) {
                const weekRow = document.createElement('tr');

                for (let j = 0; j < 7; j++) {
                    const dayCell = document.createElement('td');
                    dayCell.classList.add('px-4', 'py-8', 'text-xl', 'text-center', 'text-black', 'm-1', 'relative',
                        'hover:bg-gray-500', 'group', 'border', 'border-black');
                    if ((i % 2 === 0)) {
                        dayCell.classList.add('bg-stone-600');
                    } else {
                        dayCell.classList.add('bg-stone-700');
                    }

                    if (dayCell.textContent.trim() === '') {
                        dayCell.classList.add('empty-cell'); // Ajoute une classe à la case vide
                    }

                    const dateNumber = document.createElement('div');
                    dateNumber.classList.add('absolute', 'top-0', 'right-0', 'text-white', 'text-md', 'pr-2');

                    if (i === 0 && j < firstDayOfMonth) {
                        // Jours vides avant le premier jour du mois
                        dayCell.textContent = '';
                    } else if (dayCounter <= daysInMonth) {
                        dateNumber.textContent = dayCounter;
                        dayCell.appendChild(dateNumber);

                        const appointmentsForDay = getAppointmentsForDay(dayCounter);

                        if (appointmentsForDay.length > 0) {
                            const appointmentsContainer = document.createElement('div');
                            appointmentsContainer.classList.add('space-y-2');
                            appointmentsForDay.forEach(appointment => {
                                const appointmentDiv = document.createElement('div');
                                appointmentDiv.classList.add('my-2', 'p-4', 'rounded-md', 'shadow-sm', 'text-md',
                                    'text-white');
                                appointmentDiv.style.backgroundColor = appointment.color;

                                const appointmentContent = document.createElement('div');
                                appointmentContent.classList.add('flex', 'flex-row', 'justify-between');

                                const timeAndNameDiv = document.createElement('div');
                                timeAndNameDiv.classList.add('flex', 'flex-row');
                                const timeDiv = document.createElement('div');
                                timeDiv.textContent = moment(appointment.heure, "HH:mm:ss").format("HH:mm");
                                const nameDiv = document.createElement('div');
                                nameDiv.classList.add('px-2');
                                nameDiv.textContent = appointment.jeune.information.last_name;

                                timeAndNameDiv.appendChild(timeDiv);
                                timeAndNameDiv.appendChild(nameDiv);

                                const closeButton = document.createElement('button');
                                closeButton.innerHTML = '<i class="fa-regular fa-circle-xmark"></i>';
                                closeButton.classList.add('top-0', 'right-0');
                                closeButton.onclick = function() {
                                    console.log("delete");
                                    annulerRendezVous(appointment.id);
                                };

                                appointmentContent.appendChild(timeAndNameDiv);
                                appointmentContent.appendChild(closeButton);

                                appointmentDiv.appendChild(appointmentContent);

                                appointmentDiv.addEventListener('mouseover', (event) => {
                                    afficherInfoBulle(event, appointment);
                                });

                                appointmentDiv.addEventListener('mouseout', () => {
                                    document.querySelector('.info-bulle')?.remove();
                                });

                                appointmentsContainer.appendChild(appointmentDiv);
                            });
                            dayCell.appendChild(appointmentsContainer);
                        }

                        dayCounter++;
                    } else {
                        dayCell.textContent = '';
                    }

                    weekRow.appendChild(dayCell);
                }

                calendarBody.appendChild(weekRow);
            }
        }

        // Fonction pour générer la liste des jours pour les petits écrans
        function generateCalendarList() {
    const daysInMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1, 0).getDate();
    const calendarList = document.getElementById('calendar-list');
    calendarList.innerHTML = ''; // Supprime le contenu précédent du tableau

    for (let day = 1; day <= daysInMonth; day++) {
        const tableRow = document.createElement('tr');
        const tableData = document.createElement('td');
        tableData.textContent = day;

        if (day % 2 === 0) {
            tableData.classList.add('bg-stone-600');
        } else {
            tableData.classList.add('bg-stone-700');
        }

        const appointmentsForDay = getAppointmentsForDay(day);

        if (appointmentsForDay.length > 0) {
            const appointmentsContainer = document.createElement('div');
            appointmentsContainer.classList.add('space-y-2');
            appointmentsForDay.forEach(appointment => {
                const appointmentDiv = document.createElement('div');
                appointmentDiv.classList.add('my-2', 'p-4', 'rounded-md', 'shadow-sm', 'text-md', 'text-white');
                appointmentDiv.style.backgroundColor = appointment.color;

                const appointmentContent = document.createElement('div');
                appointmentContent.classList.add('flex', 'flex-row', 'justify-between');

                const timeAndNameDiv = document.createElement('div');
                timeAndNameDiv.classList.add('flex', 'flex-row');
                const timeDiv = document.createElement('div');
                timeDiv.textContent = moment(appointment.heure, "HH:mm:ss").format("HH:mm");
                const nameDiv = document.createElement('div');
                nameDiv.classList.add('px-2');
                nameDiv.textContent = appointment.jeune.information.last_name;

                timeAndNameDiv.appendChild(timeDiv);
                timeAndNameDiv.appendChild(nameDiv);

                const closeButton = document.createElement('button');
                closeButton.innerHTML = '<i class="fa-regular fa-circle-xmark"></i>';
                closeButton.classList.add('top-0', 'right-0');
                closeButton.onclick = function () {
                    console.log("delete");
                    annulerRendezVous(appointment.id);
                };

                appointmentContent.appendChild(timeAndNameDiv);
                appointmentContent.appendChild(closeButton);

                appointmentDiv.appendChild(appointmentContent);

                appointmentDiv.addEventListener('mouseover', (event) => {
                    afficherInfoBulle(event, appointment);
                });

                appointmentDiv.addEventListener('mouseout', () => {
                    document.querySelector('.info-bulle')?.remove();
                });

                appointmentsContainer.appendChild(appointmentDiv);
            });
            tableData.appendChild(appointmentsContainer);
        }

        tableRow.appendChild(tableData);
        calendarList.appendChild(tableRow);
    }
}




        // Fonction pour récupérer les rendez-vous pour un jour donné et les trier par heure
        function getAppointmentsForDay(day) {
            const appointments = @json($appointments);

            // Filtrer les rendez-vous pour le jour donné
            const appointmentsForDay = appointments.filter(appointment => {
                const appointmentDate = new Date(appointment.date);
                return (
                    appointmentDate.getFullYear() === currentMonth.getFullYear() &&
                    appointmentDate.getMonth() === currentMonth.getMonth() &&
                    appointmentDate.getDate() === day
                );
            });

            // Trier les rendez-vous par heure
            appointmentsForDay.sort((a, b) => {
                const heureA = new Date(`1970-01-01T${a.heure}`);
                const heureB = new Date(`1970-01-01T${b.heure}`);
                return heureA - heureB;
            });

            return appointmentsForDay;
        }

        function toggleView() {
            const calendarContainer = document.getElementById('calendar-container');
            const listContainer = document.getElementById('list-container');
            const screenWidth = window.innerWidth;

            if (screenWidth < 1024) { // Utilisez la largeur que vous préférez pour basculer entre les vues
                calendarContainer.style.display = 'none';
                listContainer.style.display = 'block';
            } else {
                calendarContainer.style.display = 'block';
                listContainer.style.display = 'none';
            }
        }

        // Appeler la fonction au chargement de la page et lors du redimensionnement de la fenêtre
        window.onload = toggleView;
        window.onresize = toggleView;


        // Fonction pour passer au mois précédent
        function previousMonth() {
            currentMonth.setMonth(currentMonth.getMonth() - 1);
            generateCalendar(); // Met à jour le calendrier pour le mois précédent
        }

        // Fonction pour passer au mois suivant
        function nextMonth() {
            currentMonth.setMonth(currentMonth.getMonth() + 1);
            generateCalendar(); // Met à jour le calendrier pour le mois suivant
        }

        // Appel initial pour afficher le calendrier du mois actuel
        generateCalendar();
        generateCalendarList();
    </script>
@endsection
