@extends('admin.templateBack')

@section('manager-content')
    <div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
        <h1 class="text-3xl font-semibold mb-6 text-center">Atelier</h1>

        <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
            @foreach ($ateliers as $atelier)
                <div class="bg-gray-800  hover:bg-gray-700 hover:text-white p-6 mb-4 border rounded-lg shadow-lg transition duration-300 transform hover:scale-105">
                    <h2 class="text-xl font-semibold mb-2">Titre:{{ $atelier->title }}</h2>
                    <p class="text-gray-400 mb-4">{{ $atelier->description }}</p>
                    
                    <!-- Nombre de candidatures en attente pour les jeunes liés au conseiller -->
                    <a href="{{ route('conseiller.ateliers.showCandidature', $atelier->id) }}"
                        class="text-blue-500 hover:underline">
                        {{ $candidaturesParAtelier[$atelier->id]['en_attente'] }} candidatures
                    </a>

                    <!-- Nombre total de candidatures acceptées -->
                    <a href="" class="text-green-500 hover:underline">
                        Acceptées: {{ $candidaturesParAtelier[$atelier->id]['accepte_total'] }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
