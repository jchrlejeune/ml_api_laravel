@extends('admin.templateBack')

@section('businessManager-content')
<div class="container mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Liste des Entreprises</h1>

    @if($entreprises && count($entreprises) > 0)
        <div class="overflow-x-auto">
            <table class="min-w-full bg-gray-800 border border-gray-600 rounded">
                <thead>
                    <tr>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Nom de l'Entreprise: </th>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Siret: </th>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Email</th>
                        <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Téléphone</th>
                        {{-- <th class="py-3 px-6 text-center bg-gray-700 font-semibold">Actions</th> --}}
                        <!-- Ajoutez d'autres colonnes si nécessaire -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($entreprises as $entreprise)
                    <tr>
                        <td class="py-4 px-6 border-t text-center">{{ $entreprise->information->company_name }}</td>
                        <td class="py-4 px-6 border-t text-center">{{ $entreprise->information->siret }}</td>
                        <td class="py-4 px-6 border-t text-center">{{ $entreprise->information->company_email }}</td>
                        <td class="py-4 px-6 border-t text-center">{{ $entreprise->information->company_phone }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
    <p class="text-center">Aucune entreprise lié à ce chargé.e d'entreprise.</p>
    @endif
</div>
@endsection