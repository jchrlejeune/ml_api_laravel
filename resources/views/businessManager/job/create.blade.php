@extends('admin.templateBack')

@section('businessManager-content')

<div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Créer une Offre d'Emploi</h1>
    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('businessManager.job.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-4">
            <label for="entreprise" class="block text-sm font-medium">Nom de l'entreprise :</label>
            <input type="text" id="entreprise" name="entreprise" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="title" class="block text-sm font-medium">Titre :</label>
            <input type="text" id="title" name="title" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job" class="block text-sm font-medium">Job visé :</label>
            <textarea id="job" name="job" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>

        <div class="mb-4">
            <label for="type" class="block text-sm font-medium">Type de Contrat :</label>
            <select id="type" name="type" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
                <option value="cdi">CDI</option>
                <option value="cdd">CDD</option>
                <option value="interim">Intérim</option>
                <option value="insertion">Contrats d’insertion</option>
                <option value="saisonnier">Saisonnier</option>
                <option value="alternance">Alternance</option>
                <option value="apprentissage">Apprentissage</option>
                <option value="professionnalisation">Professionnalisation</option>
                <option value="stage">Stage</option>
            </select>
        </div>

        <div class="mb-4">
            <label for="description" class="block text-sm font-medium">Description :</label>
            <textarea id="description" name="description" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white"></textarea>
        </div>
        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium">Image :</label>
            <input type="file" id="image_path" name="image_path" accept="image/*"
                class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>
        <div class="mb-4">
            <label for="job_pdf" class="block text-sm font-medium">PDF :</label>
            <input type="file" id="job_pdf" name="job_pdf" accept=".pdf" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="publication" class="block text-sm font-medium">Date de Publication :</label>
            <input type="date" id="publication" name="publication" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="flex justify-center mb-4">
            <button type="submit" class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Créer Offre d'Emploi
            </button>
        </div>
    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('businessManager.job.job') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>

@endsection