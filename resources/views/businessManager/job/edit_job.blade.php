@extends('admin.templateBack')

@section('businessManager-content')

<div class="container w-full lg:w-1/2 mx-auto mt-8 p-8 bg-gray-800 text-white rounded-lg shadow-lg">
    <h1 class="text-3xl font-semibold mb-6 text-center">Modifier une Offre d'Emploi</h1>
    @if(session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <!-- Pour les erreurs -->
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
    @endif

    <form action="{{ route('businessManager.job.update', $job->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT') {{-- Utilisez la méthode PUT pour indiquer une mise à jour --}}

        <div class="mb-4">
            <label for="entreprise" class="block text-sm font-medium">Nom de l'entreprise :</label>
            <input type="text" id="entreprise" name="entreprise" value="{{ old('entreprise', $job->entreprise) }}" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="title" class="block text-sm font-medium">Titre :</label>
            <input type="text" id="title" name="title" value="{{ old('title', $job->title) }}" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job" class="block text-sm font-medium">Job visé :</label>
            <textarea id="job" name="job" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ old('job', $job->job) }}</textarea>
        </div>

        <div class="mb-4">
            <label for="type" class="block text-sm font-medium">Type de Poste :</label>
            <select id="type" name="type" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
                <option value="cdi" {{ old('type', $job->type) == 'cdi' ? 'selected' : '' }}>CDI</option>
                <option value="cdd" {{ old('type', $job->type) == 'cdd' ? 'selected' : '' }}>CDD</option>
                <option value="interim" {{ old('type', $job->type) == 'interim' ? 'selected' : '' }}>Intérim</option>
                <option value="insertion" {{ old('type', $job->type) == 'insertion' ? 'selected' : '' }}>Contrats d’insertion</option>
                <option value="saisonnier" {{ old('type', $job->type) == 'saisonnier' ? 'selected' : '' }}>Saisonnier</option>
                <option value="alternance" {{ old('type', $job->type) == 'alternance' ? 'selected' : '' }}>Alternance</option>
                <option value="apprentissage" {{ old('type', $job->type) == 'apprentissage' ? 'selected' : '' }}>Apprentissage</option>
                <option value="professionnalisation" {{ old('type', $job->type) == 'professionnalisation' ? 'selected' : '' }}>Professionnalisation</option>
                <option value="stage" {{ old('type', $job->type) == 'stage' ? 'selected' : '' }}>Stage</option>
            </select>
            
        </div>

        <div class="mb-4">
            <label for="description" class="block text-sm font-medium">Description :</label>
            <textarea id="description" name="description" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">{{ old('description', $job->description) }}</textarea>
        </div>

        <div class="mb-4">
            <label for="image_path" class="block text-sm font-medium">Image :</label>
            <input type="file" id="image_path" name="image_path" accept="image/*" value="{{ old('image_path', $job->image_path) }}" class="mt-1 p-2 w-full bg-gray-800 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="job_pdf" class="block text-sm font-medium">PDF :</label>
            <input type="file" id="job_pdf" name="job_pdf" accept=".pdf" value="{{ old('job_pdf', $job->job_pdf) }}"class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="mb-4">
            <label for="publication" class="block text-sm font-medium">Date de Publication :</label>
            <input type="date" id="publication" name="publication" value="{{ old('publication', $job->publication) }}" class="mt-1 p-2 w-full bg-gray-700 border rounded-md focus:outline-none focus:ring focus:border-blue-300 text-white">
        </div>

        <div class="flex justify-center mb-4">
            <button type="submit" class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue">
                Modifier Offre d'Emploi
            </button>
        </div>
    </form>
    <div class="flex justify-center mb-4">
        <a href="{{ route('businessManager.job.job') }}"
            class="w-full text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2  rounded focus:outline-none focus:shadow-outline-blue">Retour</a>
    </div>
</div>

@endsection