<!DOCTYPE html>
<html>
<head>
    <title>Informations Utilisateur</title>
    <style>
        /* Centrer le logo */
        img.logo {
            display: block;
            margin: 0 auto; /* Centre horizontalement */
            width: 50%; /* Largeur de 50% */
        }

        /* Bandeau en largeur complète */
        img.bandeau {
            width: 100%;
        }

        /* Styles pour le formulaire */
        form {
            /* Ajoutez les styles nécessaires pour le formulaire */
        }

        /* Emplacement pour l'image du contact */
        img.contact {
            position: absolute;
            bottom: 20px; /* Ajustez la position verticale */
            left: 20px; /* Ajustez la position horizontale */
            width: 50%;
        }
    </style>
</head>
<body>
    <!-- Logo centré -->
    <img src="Images/logo1.png" alt="Logo" class="logo">

    <!-- Bandeau en largeur complète -->
    <img src="Images/bandeauML.png" alt="Bandeau" class="bandeau">
    <h1>DOSSIER D’INSCRIPTION </h1>
    <p>Prénom: {{ $information->first_name }}</p>
    <p>Nom: {{ $information->last_name }}</p>
    <p>Email: {{ $information->email }}</p>
    <p>Téléphone: {{ $information->phone }}</p>
    <p>Ville: {{ $information->city }}</p>
    <p>Âge: {{ $age }} ans</p>
   <!-- Image de contact -->
   <img src="Images/ContactML.png" alt="Contact" class="contact">
</body>
</html>
