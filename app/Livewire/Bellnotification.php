<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Livewire;
class Bellnotification extends Component
{
    public $toto;
    protected $listeners = ['NewDashboardEvent'];
    public function mount()
    {
        // Récupérer la valeur de 'toto' depuis localStorage lors du montage du composant
        $this->toto = intval(session('toto', 0));
    }

    public function increment()
    {
        $this->toto++;
        // Mettre à jour la valeur de 'toto' dans localStorage à chaque incrémentation
        session(['toto' => $this->toto]);
    }

    public function NewDashboardEvent($dashboard)
    {
        // Mettre à jour la valeur de 'toto' dans localStorage lors de la mise à jour des notifications
        $this->increment();
    }

    public function resettoto()
    {
        // Réinitialiser la valeur de 'toto' dans localStorage et dans le composant
        session(['toto' => 0]);
        $this->toto = 0;
    }

    public function render()
    {
        return view('livewire.bellnotification');
    }
}