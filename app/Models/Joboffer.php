<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Joboffer extends Model
{
    use HasFactory;

    protected $table = 'joboffer';
    
    protected $fillable = [
        'entreprise','title', 'job', 'description', 'job_pdf', 'job_url','image_path', 'image_url','publication','type',
        'actif', 'user_id'
    ];

    protected $enum = [
        'type' => [
            'CDI',
            'CDD',
            'Interim',
            'Saisonnier',
            'Alternance',
            'Apprentissage',
            'Professionnalisation',
            'Stage',
            'Insertion',
        ],
    ];

    // Définir la relation avec le modèle User (si applicable)
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    // Définir la relation avec la table "Informations"
    public function information()
    {
        // Clé étrangère : user_id dans la table Joboffer et id dans la table Informations
        return $this->belongsTo(Information::class, 'user_id', 'id');
    }

    public function getJobTitleAttribute()
    {
        return $this->attributes['job_title'];
    }

    public function showJobOffer($id)
    {
        $jobOffer = Joboffer::find($id);

        return view('/conseiller/notifications/notification', compact('jobOffer'));
    }
    
}
