<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'id',
        'type',
        'notifiable_id',
        'notifiable_type',
        'data',
        'read_at',
    ];

    protected $casts = [
        'read_at' => 'datetime',
    ];

    // Indique que la clé primaire n'est pas auto-incrémentée
    public $incrementing = false;

    // Définit le type de la clé primaire comme une chaîne (UUID)
    protected $keyType = 'string';

    // ...

    public function notifiable()
    {
        return $this->morphTo();
    }
}
