<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Joboffer;

class JobGestionBusinessManager extends Controller
{
   public function index(){



    $inactiveJob = Joboffer::where('actif', 0)->with('user.information')->get();

    return view('businessManager.job.job',compact('inactiveJob'));
   }

   public function alljob(){
    $activeJob = Joboffer::where('actif', 1)->with('information')->get();

    return view('businessManager.job.alljob',compact('activeJob'));
   }
   public function create()
   {
       return view('businessManager.job.create');
   }

   
   public function store(Request $request)
   {
       // Validation des données du formulaire
       $request->validate([
        'entreprise' => 'nullable|string|max:255',
        'title' => 'nullable|string|max:255',
        'job' => 'nullable|string',
        'type' => 'nullable|string',
        'description' => 'nullable|string',
        'image_path' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:4096', // Validation for the image (optional)
        'job_pdf' => 'nullable|mimes:pdf|max:4096', // Taille maximale de 4 Mo
        'publication' => 'nullable|date',
    ], [

        'entreprise.string' => 'Le champ "Titre" doit être une chaîne de caractères.',
        'entreprise.max' => 'Le champ "Titre" ne doit pas dépasser :max caractères.',


        'title.string' => 'Le champ "Titre" doit être une chaîne de caractères.',
        'title.max' => 'Le champ "Titre" ne doit pas dépasser :max caractères.',

        'job.string' => 'Le champ "Job visé" doit être une chaîne de caractères.',

        'type.string' => 'Le champ "Type" doit être une chaîne de caractères.',
        'type.in' => 'La valeur du champ "Type" doit être parmi :values.',

        'description.string' => 'Le champ "Description" doit être une chaîne de caractères.',

        'publication.date' => 'Le champ "Date de Publication" doit être une date valide.',

        'image_path.image' => 'Le fichier doit être une image.',
        'image_path.mimes' => 'Le fichier doit être de type jpeg ou png ou jpg ou gif.',
        'image_path.max' => 'Le fichier ne doit pas dépasser 4096 ko.',
    ]);
   
       // Création de l'offre de formation
       $job = new Joboffer;
       $job->entreprise = $request->input('entreprise');
       $job->title = $request->input('title');
       $job->job = $request->input('job');
       $job->type = $request->input('type');
       $job->description = $request->input('description');
        if ($request->hasFile('image_path')) {
            $imagePath = $request->file('image_path')->store('job_pdf/images', 'public');
            $jobImageUrl = asset("$imagePath");
            $job->image_path = $imagePath;
            $job->image_url = $jobImageUrl;
        }

        if ($request->hasFile('job_pdf')) {
            $path = $request->file('job_pdf')->store('job_pdf', 'public');
            $job_url = asset("$path");
            $job->job_pdf = $path;
            $job->job_url = $job_url;
        }
       $job->publication = $request->input('publication');
       $job->actif = 0; // Par défaut, l'offre n'est pas active
       $job->user_id = auth()->id();
   
       // Enregistrez l'offre de formation dans la base de données
       $job->save();
   
       // Redirection avec un message de succès
       return redirect()->route('businessManager.job.job')->with('success', 'Offre de formation créée avec succès.');
   }

   public function edit($id)
    {
        $job = Joboffer::findOrFail($id);
        return view('businessManager.job.edit_job', compact('job'));
    }

   public function update(Request $request, $id)
    {
        // Validation des données du formulaire
        $request->validate([
            'entreprise' => 'nullable|string|max:255',
            'title' => 'nullable|string|max:255',
            'job' => 'nullable|string',
            'type' => 'nullable|string',
            'description' => 'nullable|string',
            'image_path' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:4096', // Validation for the image (optional)
            'job_pdf' => 'nullable|mimes:pdf|max:4096', // Taille maximale de 4 Mo
            'publication' => 'nullable|date',
        ], [

            'entreprise.string' => 'Le champ "Titre" doit être une chaîne de caractères.',
            'entreprise.max' => 'Le champ "Titre" ne doit pas dépasser :max caractères.',

            'title.string' => 'Le champ "Titre" doit être une chaîne de caractères.',
            'title.max' => 'Le champ "Titre" ne doit pas dépasser :max caractères.',


            'job.string' => 'Le champ "Job visé" doit être une chaîne de caractères.',


            'type.string' => 'Le champ "Type de Poste" doit être une chaîne de caractères.',
            'type.in' => 'La valeur du champ "Type de Poste" doit être parmi :values.',

            'description.string' => 'Le champ "Description" doit être une chaîne de caractères.',

            'publication.date' => 'Le champ "Date de Publication" doit être une date valide.',

            'image_path.image' => 'Le fichier doit être une image.',
            'image_path.mimes' => 'Le fichier doit être de type jpeg ou png ou jpg ou gif.',
            'image_path.max' => 'Le fichier ne doit pas dépasser 4096 ko.',

            'job_pdf.mimes' => 'Le fichier doit être de type pdf.',
            'job_pdf.max' => 'Le fichier ne doit pas dépasser 4096 ko.',
        ]);

        // Récupérer l'offre de formation à mettre à jour
        $job = Joboffer::findOrFail($id);
        $job->entreprise = $request->input('entreprise');
        $job->title = $request->input('title');
        $job->job = $request->input('job');
        $job->type = $request->input('type');
        $job->description = $request->input('description');
        $job->publication = $request->input('publication');

        if ($request->hasFile('job_pdf')) {
            if ($job->job_pdf) {
                Storage::disk('public')->delete($job->job_pdf);
            }
            $path = $request->file('job_pdf')->store('job_pdf', 'public');
            $job_url = asset("$path");
            $job->job_pdf = $path;
            $job->job_url = $job_url;
        }

        if ($request->hasFile('image_path')) {
            if ($job->image_path) {
                Storage::disk('public')->delete($job->image_path);
            }
            $imagePath = $request->file('image_path')->store('job_pdf/images', 'public');
            $jobImageUrl = asset("$imagePath");
            $job->image_path = $imagePath;
            $job->image_url = $jobImageUrl;
        }

        // Enregistrez les modifications dans la base de données
        $job->save();

        // Redirection avec un message de succès
        return redirect()->route('businessManager.job.job')->with('success', 'Offre d\'emploi mise à jour avec succès.');
    }

   public function toggle($id)
   {
       $job = Joboffer::findOrFail($id);
       $job->actif = !$job->actif; // Inversez la valeur d'actif (1 devient 0 et vice versa)
       $job->save();

       return redirect()->back()->with('success', "Offre d'emploi activée/désactivée avec succès.");
   }

   public function delete($id)
{
    $job = Joboffer::findOrFail($id);
    $job->delete();

    return redirect()->back()->with('success', 'Offre d\'emploi supprimée avec succès.');
}
}
