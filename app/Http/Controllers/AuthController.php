<?php
 
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
 
use Illuminate\Http\Request;
use App\Exceptions\AuthenticationFailedException;
 
class AuthController extends Controller
{
    public function showJeuneLoginForm()
    {
        return view('auth.jeune_login');
    }
 
    public function jeuneLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
    
        if (Auth::attempt($credentials)) {
            $role = Auth::user()->role;
            $user = Auth::user();
    
            // Load the related information
            $user->load('information');
    
            // Generate Sanctum token and get its expiration time
            $token = $user->createToken('auth_token', ['expires_in' => config('sanctum.expiration')]);
            //$expiration = $token->accessToken->expires_at;
            $expiration = $token->accessToken->created_at->addMinutes(config('sanctum.expiration'));
    
            switch ($role) {
                case 'admin':
                    return redirect()->route('admin.templateBack');
                case 'manager':
                    return redirect()->route('conseiller.user.user');
                case 'businessManager':
                    return redirect()->route('businessManager.entreprise');
                case 'jeune':
                    $response = [
                        'message' => 'Connecté avec succès en tant que jeune',
                        'role' => $role,
                        'information' => $user->information,
                        'documents' => $user->documents,
                        'rendezVous' => $user->rendezVous()->with('conseiller.information')->get(),
                        'formations' => $user->userTrainings()->withPivot('status')->get(),
                        'ateliers' => $user->userAteliers()->withPivot('status')->get(),
                        'jobOffers' => $user->userJobOffer()->withPivot('status')->get(),
                        'token' => $token->plainTextToken,
                        'expires_at' => $expiration ? $expiration->toIso8601String() : null,
                    ];
        
                    if ($request->wantsJson()) {
                        return response()->json($response, 200);
                    } else {
                        throw new AuthenticationFailedException();
                    }
                case 'entreprise':
                    $response = [
                        'message' => "Connecté avec succès en tant qu'entreprise",
                        'role' => $role,
                        'information' => $user->information,
                        'documents' => null,
                        'rendezVous' => null,
                        'formations' => null,
                        'ateliers' => null,
                        'jobOffers' => null,
                        'token' => $token->plainTextToken,
                        'expires_at' => $expiration ? $expiration->toIso8601String() : null,
                    ];
        
                    if ($request->wantsJson()) {
                        return response()->json($response, 200);
                    } else {
                        throw new AuthenticationFailedException();
                    }
                default:
                throw new AuthenticationFailedException();
            }
        }
        throw new AuthenticationFailedException();
    }
}