<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\ConseillerJeune;
use App\Models\Information;
use Illuminate\Support\Facades\DB;


class UserGestion extends Controller
{
    
    //--------------------------------------Admin
    public function index()
    {
        $usersActifs = User::where('actif', 1)->where('role', '!=', 'admin')->where('role', 'jeune')->paginate(10);
        $usersInactifs = User::where('actif', 0)->where('role', '!=', 'admin')->where('role', 'jeune')->paginate(10);
        $allUserIds = $usersActifs->pluck('id')->merge($usersInactifs->pluck('id'));
        $conseillers = ConseillerJeune::whereIn('jeune_id', $allUserIds)->get();
        
        return view('admin.user.user', compact('usersActifs', 'usersInactifs','conseillers'));
    }

    public function toggleActivation($id)
    {
        $user = User::find($id);
        $user->actif = !$user->actif; // Inversez l'état actif de l'utilisateur (0 devient 1, et vice versa)
        $user->save();
        return redirect()->back()->with('success', 'État actif de l\'utilisateur mis à jour.');
    }

    
    public function create()
    {
        return view('admin.user.create');
    }

    public function edit($id)
    {
        // Récupérer la formation par son ID
        $user = User::findOrFail($id);
        $conseillers = User::where('role', 'manager')->get();
        if (!$user) {
        return redirect()->route('admin.user.user')->with('error', "l'utilisateur n'existe pas");
        }
        Log::info($user);
        return view('admin.user.edit_user', compact('user','conseillers'));
    }

    public function update(Request $request, $id)
    {
        // Récupération de l'utilisateur
        $user = User::findOrFail($id);

        // Définition des règles de validation
        $rules = [
            'first_name' => 'required|regex:/^[a-zA-ZÀ-ÿ -]+$/',
            'last_name' => 'required|regex:/^[a-zA-ZÀ-ÿ -]+$/',
            'phone' => 'required|regex:/^[+0-9 -]+$/|between:10,20',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'nullable|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
        ];

        $customMessages = [
            'first_name.required' => 'Le champ Prénom est requis.',
            'first_name.regex' => 'Le Prénom doit contenir  des lettres, les espaces et les tirets sont acceptés.',

            'last_name.required' => 'Le champ Nom est requis.',
            'last_name.regex' => 'Le Nom doit contenir uniquement des lettres.',

            'phone.required' => 'Le champ Téléphone est requis.',
            'phone.min' => 'Le Téléphone doit contenir au moins 10 chiffres',
            'phone.min' => 'Le Téléphone doit contenir au maximum 20 chiffres',
            'phone.regex' => 'Le Téléphone doit contenir uniquement des chiffres.',

            'email.required' => 'Le champ Email est requis.',
            'email.email' => 'Veuillez saisir une adresse email valide.',
            'email.unique' => 'Cet email est déjà pris.',

            'password.min' => 'Le mot de passe doit comporter au moins 8 caractères.',
            'password.regex' => 'Le mot de passe doit contenir au moins une majuscule, une minuscule et un chiffre.',
        ];
        // Validation des données du formulaire
        $request->validate($rules, $customMessages);
        // Utilisation d'une transaction pour assurer la cohérence des mises à jour
        DB::transaction(function () use ($request, $user) {
            // Mise à jour des informations de l'utilisateur dans la table informations
            $user->information->update([
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
            ]);
            // Mise à jour de l'email dans la table users
            $user->email = $request->input('email');
            // Mise à jour du mot de passe s'il est fourni
            if ($request->filled('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();
        });

        // Mise à jour du conseiller s'il est fourni
        if ($request->filled('conseiller_id')) {
            // Mettez à jour l'entrée dans la table pivot
            ConseillerJeune::where('jeune_id', $user->id)
                ->update(['conseiller_id' => $request->input('conseiller_id')]);
        }
        return redirect()->route('admin.user.user')->with('success', 'Mis à jour faite avec succès.');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        Log::info($user);
        if ($user) {
            $user->delete();
            return redirect()->route('admin.user.user')->with('success', 'Formation supprimée avec succès.');
        } else {
            return redirect()->route('admin.user.user')->with('error', 'Formation non trouvée.');
        }
    }
    



    //----------------------------------BusinessManager

    public function index_businessManager()
    {
        $usersActifs = User::where('actif', 1)->where('role','entreprise')->paginate(10);
        $usersInactifs = User::where('actif', 0)->where('role','entreprise')->paginate(10);
        return view('admin.user.user', compact('usersActifs', 'usersInactifs'));
    }

    public function toggleActivation_businessManager($id)
    {
        $user = User::find($id);
        $user->actif = !$user->actif; // Inversez l'état actif de l'utilisateur (0 devient 1, et vice versa)
        $user->save();
        return redirect()->back()->with('success', 'État actif de l\'utilisateur mis à jour.');
    }
}
