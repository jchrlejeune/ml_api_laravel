<?php

namespace App\Http\Controllers;

use App\Notifications\CandidatureNotification;
use App\Notifications\FormulaireNotification;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Training;
use App\Models\UserTraining;
use App\Models\UserAtelier;
use App\Models\User;
use App\Models\Atelier;
use App\Models\Joboffer;
use App\Models\ConseillerJeune;
use App\Models\Staff;
use App\Models\News;
use App\Models\PhotoGroupe;
use App\Models\UserJoboffer;



class FrontController extends Controller
{
    public function home()
    {
        $latestNews = News::orderBy('created_at', 'desc')->take(3)->get();
        return view('home', compact('latestNews'));
    }


    public function homeFront()
    {
        try {
            $latestNews = News::orderBy('created_at', 'desc')->take(3)->get();
    
            return response()->json(['latestNews' => $latestNews], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred while fetching data.'], 500);
        }
    }

    public function etreAccompagne()
    {
        return view('etreAccompagne');
    }


    public function noscommunes()
    {
        return view('noscommunes');
    }

    public function nosmissions()
    {
        return view('nosmissions');
    }

    public function orienter()
    {
        return view('orienter');
    }

    public function seformer()
    {
        return view('seformer');
    }

    public function trouverUnEmploi()
    {
        return view('trouverUnEmploi');
    }

    public function espacepersoDetail()
    {
        try {
            // Check if the user is authenticated
            if (Auth::check()) {
                // User is authenticated, proceed with retrieving data
                $user = Auth::user();
                $information = $user->information;
                $documents = $user->documents;
                $rendezVous = $user->rendezVous()->with('conseiller.information')->get();
                $formations = $user->userTrainings()->withPivot('status')->get();
                $ateliers = $user->userAteliers()->withPivot('status')->get();
                $jobOffers = $user->userJobOffer()->withPivot('status')->get();
                
                return response()->json([
                    'information' => $information,
                    'documents' => $documents,
                    'rendezVous' => $rendezVous,
                    'formations' => $formations,
                    'ateliers' => $ateliers,
                    'jobOffers' => $jobOffers,
                ], 200);
            } else {
                // User is not authenticated
                throw new \Exception('User not authenticated', 401);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


        public function actualite(Request $request)
    {
        $perPage = $request->query('per_page', 6);
        $actualites = News::orderBy('created_at', 'desc')->paginate($perPage);

        // Assuming you want to include pagination information in the response
        return response()->json([
            'actualites' => $actualites->items(),
            'pagination' => [
                'total' => $actualites->total(),
                'per_page' => $actualites->perPage(),
                'current_page' => $actualites->currentPage(),
                'last_page' => $actualites->lastPage(),
            ],
        ]);
    }

        public function show($id)
    {
        // $actualite = News::findOrFail($id); // Récupère l'actualité correspondant à l'ID

        // return view('actualiteDetail', compact('actualite'));

        try {
            $actualite = News::findOrFail($id);

            return response()->json(['actualite' => $actualite], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Actualité not found.'], 404);
        }
    }

        public function ateliers(Request $request)
    {
        $perPage = $request->query('per_page', 6);
        $ateliers = Atelier::where('actif', 1)->orderBy('date')->paginate($perPage);

        return response()->json([
            'ateliers' => $ateliers,
            'pagination' => [
                'total' => $ateliers->total(),
                'per_page' => $ateliers->perPage(),
                'current_page' => $ateliers->currentPage(),
                'last_page' => $ateliers->lastPage()
            ],
        ], 200);
    }

        public function atelierDetails($id)
    {
        try {
            $atelier = Atelier::findOrFail($id);
    
            // Conversion de la date en objet Carbon pour manipulation
            $date = \Carbon\Carbon::parse($atelier->date);
            // Extraction du jour, mois et année
            $day = $date->format('d');
            $month = $date->format('m');
            $year = $date->format('Y');
    
            $data = [
                'atelier' => $atelier,
                'day' => $day,
                'month' => $month,
                'year' => $year,
            ];
    
            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Atelier not found'], 404);
        }
    }

        public function testRoute()
    {
        return response()->json(['message' => 'Testing route without ID parameter.']);
    }


    //Inscription Atelier
    public function inscription($atelierId)
        {
        // Check if the user is authenticated
        if (Auth::guard('sanctum')->check()) {
                $user = Auth::guard('sanctum')->user();
                $atelier = Atelier::find($atelierId);
                // Check if the atelier exists
                if (!$atelier) {
                    return response()->json(['error' => 'Atelier not found.'], 404);
                }

                // Check if the user is already registered for this workshop
                if ($atelier->users()->where('user_id', $user->id)->exists()) {
                        return response()->json(['error' => 'Vous êtes déjà inscrit à cet atelier.'], 400);
                    }

                    // Check if the workshop has reached the maximum number of slots
                    $acceptedUsersCount = $atelier->acceptedUsersCount();

                    if ($atelier->slots !== null && $acceptedUsersCount >= $atelier->slots) {
                        return response()->json(['error' => 'Désolé, plus de place disponible pour cet atelier.'], 400);
                    }

                // Register the user for the workshop with the status 'en attente'
                $atelier->users()->attach($user->id, ['status' => 'en attente']);

                return response()->json(['success' => 'Demande d\'inscription envoyée pour l\'atelier.'], 200);
        } else {
            // Redirect for non-authenticated users
            return response()->json(['error' => 'Veuillez vous connecter pour vous inscrire à l\'atelier.'], 401);
        }
    }


    public function notreexpertise()
    {

        return view('notreexpertise');
    }

    public function taxeapprentissage()
    {

        return view('taxeapprentissage');
    }

    public function demarcheRSE()
    {

        return view('demarcheRSE');
    }

    public function espacepersonneljeune()
    {

        return view('espacepersonneljeune');
    }

    public function inscriptionjeune()
    {

        return view('inscriptionjeune');
    }

    public function inscriptionentreprise()
    {

        return view('inscriptionentreprise');
    }

    public function preinscription()
    {

        return view('preinscription');
    }

    public function espacepersonnel()
    {

        return view('espacepersonnel');
    }


    public function contact()
    {

        return view('contact');
    }

    public function espaceentrepriseDetail()
    {

        return view('espaceentrepriseDetail');
    }

    public function formation(Request $request)
    {
        $perPage = $request->query('per_page', 6);
        $formations = Training::orderBy('created_at', 'desc')->paginate($perPage);
        return response()->json([
            'formations' => $formations->items(),
            'pagination' => [
                'total' => $formations->total(),
                'per_page' => $formations->perPage(),
                'current_page' => $formations->currentPage(),
                'last_page' => $formations->lastPage(),
            ],
        ]);
    }

        public function formationDetail($id)
    {
        try {
            $formation = Training::findOrFail($id);
            return response()->json(['formation' => $formation], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Formation not found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Something went wrong'], 500);
        }
    }

    //postuler formation
        public function postuler($formationId)
    {
    // Check if the user is authenticated
        if (Auth::guard('sanctum')->check()) {
        $user = Auth::guard('sanctum')->user();

            // Retrieve the formation with the given ID
            $formation = Training::find($formationId);

            // Check if the formation exists
            if (!$formation) {
                return response()->json(['error' => 'Formation not found.'], 404);
            }

        // Check if the user is already registered for this formation
        $isRegistered = $formation->users()->where('user_id', $user->id)->exists();

        if ($isRegistered) {
            return response()->json(['error' => 'Vous êtes déjà inscrit à cette formation.'], 400);
        }

        // Check if the maximum number of participants has been reached
        $acceptedUsersCount = $formation->acceptedUsersCount();

        if ($formation->slots !== null && $acceptedUsersCount >= $formation->slots) {
            return response()->json(['error' => 'Désolé, plus de place disponible pour cette formation.'], 400);
        }

        // Register the user for the formation with the status 'en attente' (pending)
        $formation->users()->attach($user, ['status' => 'en attente']);
        return response()->json(['success' => 'Demande d\'inscription envoyée pour la formation.'], 200);
    } else {
        // Redirect non-authenticated users to the login/registration page
        return response()->json(['error' => 'Veuillez vous connecter pour vous inscrire à cette formation.'], 401);
    }
}

    public function emploi(Request $request)
    {
        try {
            // Set the default number of items per page
            $perPage = $request->get('per_page', 10);
            $jobOffers = JobOffer::where('actif', 1)->paginate($perPage);
            return response()->json([
                'emploi' => $jobOffers->items(),
                'pagination' => [
                    'total' => $jobOffers->total(),
                    'per_page' => $jobOffers->perPage(),
                    'current_page' => $jobOffers->currentPage(),
                    'last_page' => $jobOffers->lastPage(),
                ],
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


    public function postulerOffre( $offreId)
    {
        if (Auth::guard('sanctum')->check()) {
            $jeune = Auth::guard('sanctum')->user();
            $offre = JobOffer::findOrFail($offreId);

            if ($jeune) {
                $conseiller = ConseillerJeune::where('jeune_id', $jeune->id)->first()->conseiller ?? null;

                if ($conseiller) {
                    // Check if a similar notification already exists
                    $existingNotification = $conseiller->notifications()
                        ->where('type', CandidatureNotification::class)
                        ->where('data->jeune_id', $jeune->id)
                        ->where('data->offre_id', $offre->id)
                        ->exists();

                    if (!$existingNotification) {
                        $notification = new CandidatureNotification($jeune, $offre);
                        $data = $notification->toArray($conseiller);

                        // Create an instance of the notification without saving
                        $notificationModel = $conseiller->notifications()->make([
                            'type' => get_class($notification),
                            'notifiable_id' => $conseiller->id,
                            'notifiable_type' => get_class($conseiller),
                            'data' => json_encode($data),
                        ]);

                        // Manually assign the UUID key
                        $notificationModel->id = \Illuminate\Support\Str::uuid();

                        // Save the notification
                        $notificationModel->save();

                        // Save the user_job_offer
                        $userJobOffer = new UserJoboffer();
                        $userJobOffer->user_id = $jeune->id;
                        $userJobOffer->job_offer_id = $offre->id;
                        $userJobOffer->type = 'App\Notifications\CandidatureNotification';
                        $userJobOffer->status = 'en attente';
                        $userJobOffer->save();

                        return response()->json(['success' => 'Votre conseiller a bien reçu votre demande.'], 200);
                    } else {
                        return response()->json(['error' => 'Vous avez déjà postulé pour cette offre.'], 409);
                    }
                }
            }

            return response()->json(['error' => 'Une erreur s\'est produite lors de la notification du conseiller.'], 500);
        } else {
            return response()->json(['error' => 'Utilisateur non authentifié.'], 401);
        }
    }


    public function gouvernance()
    {
        $conseilAdmin = Staff::where('group', 'Conseil d\'administration')->get(['first_name', 'last_name', 'job', 'picture', 'image_url']);
        $bureau = Staff::where('group', 'Bureau')->get(['first_name', 'last_name', 'job', 'picture', 'image_url']);

        return response()->json(['conseil_admin' => $conseilAdmin, 'bureau' => $bureau]);
    }

        public function equipe()
    {
        try {
            // Récupérer les membres pour chaque pôle
            $direction = Staff::where('group', 'Pôle Direction')->get();
            $structure = Staff::where('group', 'Pôle Structure')->get();
            $technique = Staff::where('group', 'Pôle Technique')->get();

            $photogroupe = PhotoGroupe::latest()->get();

            // Créer un tableau associatif pour la réponse JSON
            $data = [
                'direction' => $direction,
                'structure' => $structure,
                'technique' => $technique,
                'photogroupe' => $photogroupe,
            ];

            // Retourner une réponse JSON
            return response()->json($data);
        } catch (\Exception $e) {
            // Handle the exception here
            return response()->json(['error' => 'An error occurred while processing the request.'], 500);
        }
    }

    public function notifierConseillerFormulaire(Request $request)
    {
        $data = $request->json()->all();
        Log::info($data);
        try {
            // Check if the user is authenticated
            if (Auth::guard('sanctum')->check()) {
                $jeuneId = $data['jeune_id'];
                $jeune = User::where('id', $jeuneId)->where('role', 'jeune')->first();
                Log::info($jeune);
                $conseiller = ConseillerJeune::getConseillerByJeuneId($jeune->id);
                Log::info($conseiller);

                if ($conseiller) {
                    $existingNotification = $conseiller->notifications()
                        ->where('type', FormulaireNotification::class)
                        ->where('data->jeune_id', $data['jeune_id'])
                        ->where('data->choix', $data['choix'])
                        ->exists();
                    if (!$existingNotification) {
                        $notification = new FormulaireNotification($jeune, json_encode($data));

                        // Créer une instance de notification pour le conseiller
                        $notificationModel = $conseiller->notifications()->make([
                            'type' => get_class($notification),
                            'notifiable_id' => $conseiller->id,
                            'notifiable_type' => get_class($conseiller),
                            'data' => json_encode($data), // Convertir les données en JSON
                        ]);

                        // Attribution de la clé UUID manuellement
                        $notificationModel->id = \Illuminate\Support\Str::uuid();
                        $notificationModel->save();

                        return response()->json(['success' => 'Votre conseiller a bien reçu votre demande.'], 200);
                    } else {
                        return response()->json(['error' => 'Vous avez déjà envoyé une demande à votre conseiller. Il vous contactera dans les plus brefs délais.'], 409);
                    }
                }
            } else {
                // User is not authenticated
                throw new \Exception('User not authenticated', 401);
            }
        } catch (\Exception $e) {
            Log::error('Exception occurred: ' . $e->getMessage());
            Log::error($e);
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }
    }

    public function notifierBusinessManagerFormulaire(Request $request)
    {
        $data = $request->all();
        Log::info( $data);
        try {
            // Validation des données du formulaire
            $request->validate([
                'entreprise' => 'nullable|string|max:255',
                'title' => 'nullable|string|max:255',
                'job' => 'nullable|string',
                'type' => 'nullable|string',
                'description' => 'nullable|string',
                'image_path' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:4096', // Validation for the image (optional)
                'job_pdf' => 'nullable|mimes:pdf|max:4096', // Taille maximale de 4 Mo
                'publication' => 'nullable|date',
            ]);
    
            // Création de l'offre de formation
            $job = new Joboffer;
            $job->entreprise = $request->input('entreprise');
            $job->title = $request->input('title');
            $job->job = $request->input('job');
            $job->type = $request->input('type');
            $job->description = $request->input('description');
            Log::info($request->input('image'));
            Log::info($request->input('docpdf'));
  
            // Gérer l'image si fournie
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                // Assurez-vous que le fichier est une image
                if ($image->isValid() && $image->getClientMimeType() === 'image/jpeg') {
                    $imageName = 'image_' . time() . '.' . $image->getClientOriginalExtension();
                    // Déplacer l'image vers le dossier approprié
                    $image->move(public_path('job_pdf/images'), $imageName);
                    // Construire l'URL de l'image
                    $imageUrl = url('job_pdf/images/' . $imageName);
                    $job->image_path = $imageName;
                    $job->image_url = $imageUrl;
                } else {
                    // En cas de format de fichier invalide, enregistrez null
                    $job->image_path = null;
                    $job->image_url = null;
                }
            } else {
                // Aucune image fournie, enregistrez null
                $job->image_path = null;
                $job->image_url = null;
            }

            // Gérer le PDF si fourni
            if ($request->hasFile('docpdf')) {
                $pdf = $request->file('docpdf');
                // Assurez-vous que le fichier est bien un PDF
                if ($pdf->isValid() && $pdf->getClientMimeType() === 'application/pdf') {
                    $pdfName = 'pdf_' . time() . '.' . $pdf->getClientOriginalExtension();
                    // Déplacer le PDF vers le dossier approprié
                    $pdf->move(public_path('job_pdf'), $pdfName);
                    // Construire l'URL du PDF
                    $pdfUrl = url('job_pdf/' . $pdfName);
                    $job->job_pdf = $pdfName;
                    $job->job_url = $pdfUrl;
                } else {
                    // En cas de format de fichier invalide, enregistrez null
                    $job->job_pdf = null;
                    $job->job_url = null;
                }
            } else {
                // Aucun PDF fourni, enregistrez null
                $job->job_pdf = null;
                $job->job_url = null;
            }

    
            $job->publication = $request->input('publication');
            $job->actif = 0; // Par défaut, l'offre n'est pas active
            $job->user_id = auth()->id();
    
            // Enregistrez l'offre de formation dans la base de données
            $job->save();
    
             // Renvoyer une réponse JSON avec un message de succès
            return response()->json(["success' => 'Offre d\'emploi créée avec succès."], 200);
        } catch (\Exception $e) {
             // Récupérer les détails de l'exception
            $errorMessage = $e->getMessage();
            $errorLine = $e->getLine();
            $errorFile = $e->getFile();

            // Loguer les détails de l'exception
            Log::error("Exception occurred: $errorMessage at line $errorLine in file $errorFile");

            // Renvoyer une réponse JSON avec un message d'erreur détaillé
            return response()->json(['error' => "Une erreur est survenue lors de la création de l\'offre d\'emploi.", 'details' => [
                'message' => $errorMessage,
                'line' => $errorLine,
                'file' => $errorFile
            ]], 500);
        }
    }
    

    public function getUserData()
    {
        $user = Auth::user();
        if ($user) {
            $user->load('information');
            $role = $user->role;
            switch ($role) {
                case 'jeune':
                    return response()->json([
                        'role' => $role,
                        'information' => $user->information,
                        'documents' => $user->documents,
                        'rendezVous' => $user->rendezVous()->with('conseiller.information')->get(),
                        'formations' => $user->userTrainings()->withPivot('status')->get(),
                        'ateliers' => $user->userAteliers()->withPivot('status')->get(),
                        'jobOffers' => $user->userJobOffer()->withPivot('status')->get(),
                    ], 200);

                case 'entreprise':
                    return response()->json([
                        'role' => $role,
                        'information' => $user->information,
                        'documents' => null,
                        'rendezVous' => null,
                        'formations' => null,
                        'ateliers' => null,
                        'jobOffers' => null,
                    ], 200);

                default:
                    return response()->json(['error' => "Rôle non reconnu."], 400);
            }
        }

        return response()->json(['error' => "Utilisateur non authentifié."], 401);
    }
        
}
