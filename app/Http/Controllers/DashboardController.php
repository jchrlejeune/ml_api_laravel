<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dashboard;
use App\Events\NewDashboardEvent;
use Illuminate\Support\Facades\Event;
use Livewire\Livewire;


class DashboardController extends Controller
{
    //
    public function index()
    {
        //Récupérer tous les membres du personnel depuis la base de données
        $dashboard = Dashboard::all();
        return view('admin.dashboard.index', compact('dashboard'));

    }

    public function store(Request $request)
    {
        // Validation des données du formulaire
        $rules = [
            'test' => 'nullable|string',
        ];

        // Custom error messages for staff creation
        $customMessages = [
            'test.string' => 'Le test doit être une chaîne de caractère.',
        ];
        
        $request->validate($rules, $customMessages);

        $dashboard = Dashboard::create([
            'test' => $request->input('test'),
        ]);

        event(new NewDashboardEvent($dashboard));
        
        return redirect()
            ->route('admin.dashboard.index')
            ->with('success', 'test créée avec succès.');
    }

    public function destroy(Request $request)
    {
        // Récupère tous les enregistrements
        $dashboards = Dashboard::all();

        // Supprime chaque enregistrement individuellement
        foreach ($dashboards as $dashboard) {
            $dashboard->delete();
        }

        return redirect()->route('admin.dashboard.index')->with('success', 'Tous les enregistrements ont été supprimés avec succès.');
    }
}
