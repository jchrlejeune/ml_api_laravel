<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\UserJobOffer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    public function index()
{
    $conseiller_id = auth()->user()->id;
    $notifications = Notification::all();
    Log::info($notifications);

    return view('conseiller.notifications.notifications', compact('notifications'));
}

    public function markAsRead($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->read_at = now();
        $notification->save();

        return redirect()->back()->with('success', 'Notification marquée comme lue.');
    }

    public function delete($uuid)
    {
        $notification = Notification::findOrFail($uuid);

        if ($notification->type === 'App\Notifications\CandidatureNotification') {

            $data = json_decode($notification->data, true);
            Log::info($data);
            if ($data['offre_id']) {
                $offreId = $data['offre_id'];
                Log::info($offreId);
                Log::info('delete user_job_offer '.$offreId);
                UserJobOffer::where('job_offer_id', $offreId)->delete();
            }
        }
        $notification->delete();
        Log::info('deleted');
        return redirect()->route('conseiller.notification.index')->with('success', 'Notification supprimée avec succès.');
    }
}