<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Information;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class EntrepriseGestion extends Controller
{
    public function index()
    {
        // Récupérer les utilisateurs actifs avec leurs informations liées
        $usersActifs = User::where('actif', 1)->where('role', 'entreprise')->with('information')->get();

        // Récupérer les utilisateurs inactifs avec leurs informations liées
        $usersInactifs = User::where('actif', 0)->where('role', 'entreprise')->with('information')->get();

        return view('admin.company.company', compact('usersActifs', 'usersInactifs'));
    }

    public function create()
    {
        return view('admin.company.create');
    }
    public function edit($id)
    {
        $user = User::findOrFail($id);
    
        // Vérifiez si l'utilisateur existe
        if (!$user) {
            return redirect()->route('admin.company.company')->with('error', "L'utilisateur n'existe pas");
        }
    
        // Passez la variable $user à votre vue
        return view('admin.company.edit_user', compact('user'));
    }
    public function update(Request $request, $id)
    {
        // Récupération de l'utilisateur
        $user = User::findOrFail($id);
    
        // Définition des règles de validation
        $rules = [
            'company_phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'company_name' => 'required',
            'responsible_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id, // L'email doit être unique dans la table users, sauf pour l'utilisateur actuel
            'password' => 'required|min:6', // 6 caractères minimum
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10', // 10 chiffres
            'siret' => 'required|regex:/^[0-9]{14}$/', // 14 chiffres
            'company_address' => 'required', // Adresse de l'entreprise
        ];
    
        // Personnalisation des messages d'erreur
        $customMessages = [
            'required' => 'Le champ :attribute est obligatoire.',
            'regex' => 'Le champ :attribute n\'est pas valide.',
            'email' => 'Le champ :attribute doit être une adresse email valide.',
            'unique' => 'Le champ :attribute est déjà utilisé.',
            'min' => 'Le champ :attribute doit contenir au moins :min caractères.',
        ];
        // Validation des données du formulaire
        $request->validate($rules, $customMessages);
        // Utilisation d'une transaction pour assurer la cohérence des mises à jour
        DB::transaction(function () use ($request, $user) {
            // Mise à jour des informations de l'utilisateur dans la table informations
            $user->information->update([
                'first_name' => $request->input('first_name'),
                'name' => $request->input('name'),
                'company_phone' => $request->input('company_phone'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'city' => $request->input('city'),
                'zip' => $request->input('zip'),
                'siret' => $request->input('siret'),
            ]);
            // Mise à jour de l'email dans la table users
            $user->email = $request->input('email');
            // Mise à jour du mot de passe s'il est fourni
            if ($request->filled('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();
        });
        Log::info('L\'entreprise ' . $user->name . ' a mis à jour ses informations.');
        // Rediriger l'utilisateur vers la page de gestion des utilisateurs avec un message de succès
        return redirect()->route('admin.company.company')->with('success', 'Informations de l\'entreprise mises à jour.');
    }
    
}
