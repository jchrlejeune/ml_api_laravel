<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\ConseillerJeune;
use App\Models\Training;
use App\Models\UserTraining;
use App\Models\Atelier;
use App\Models\UserAtelier;
use App\Models\User;
use App\Models\Information;



class ConseillerController extends Controller
{
    // public function index()
    // {
    //     //Récupérer tous les membres du personnel depuis la base de données
    //     $dashboard = ConseillerJeune::all();
    //     return view('conseiller.dashboard.index', compact('dashboard'));
    // }

    // public function store(Request $request)
    // {
    //     $rules = [
    //         'notification' => 'nullable|string',
    //     ];

    //     $customMessages = [
    //         'notification.string' => 'La notification doit être une chaîne de caractère.',
    //     ];

    //     $request->validate($rules, $customMessages);

    //     $conseillerJeune = auth()->user()->conseillerJeune;

    //     if ($conseillerJeune) {
    //         // Créer une nouvelle notification
    //         $conseillerJeune->create(['notification' => $request->input('notification')]);

    //         return redirect()
    //             ->route('conseiller.dashboard.index')
    //             ->with('success', 'Notification créée avec succès.');
    //     } else {
    //         // Gérer le cas où la relation conseiller_jeune n'existe pas
    //         return redirect()
    //             ->route('conseiller.dashboard.index')
    //             ->with('error', 'Erreur: Relation conseiller_jeune non trouvée.');
    //     }
    // }

    // public function destroy(Request $request)
    // {
    //     $dashboard = ConseillerJeune::all();

    //     foreach ($dashboard as $item) {
    //         $item->delete();
    //     }

    //     return redirect()->route('conseiller.dashboard.index')->with('success', 'Toutes les notifications ont été supprimées avec succès.');
    // }


    //---------------
    // public function notifications()
    // {
    //     $conseillerId = auth()->user()->id;

    //     $notifications = Notification::where('notifiable_id', $conseillerId)
    //         ->where('notifiable_type', 'App\Models\User')
    //         ->get();

    //     return view('conseiller.notifications.index', compact('notifications'));
    // }

    
        //-------------------------------------Manager

        public function index()
        {
            $usersActifs = User::where('actif', 1)->where('role','jeune')->paginate(10);
            $usersInactifs = User::where('actif', 0)->where('role','jeune')->paginate(10);
            $allUserIds = $usersActifs->pluck('id')->merge($usersInactifs->pluck('id'));
            $conseiller = auth()->user();
            return view('conseiller.user.user', compact('usersActifs', 'usersInactifs','conseiller'));
        }
    
        public function toggleActivation_manager($id)
        {
            $user = User::find($id);
            $user->actif = !$user->actif; // Inversez l'état actif de l'utilisateur (0 devient 1, et vice versa)
            $user->save();
            return redirect()->back()->with('success', 'État actif de l\'utilisateur mis à jour.');
        }
    
        public function create_manager()
        {
            return view('conseiller.user.create');
        }
    
        public function edit_manager($id)
        {
            // Récupérer la formation par son ID
            $user = User::findOrFail($id);
            $conseillers = User::where('role', 'manager')->get();
            if (!$user) {
            return redirect()->route('conseiller.user.user')->with('error', "l'utilisateur n'existe pas");
            }
            Log::info($user);
            return view('conseiller.user.edit_user', compact('user','conseillers'));
        }
    
        public function update_manager(Request $request, $id)
        {
            // Récupération de l'utilisateur
            $user = User::findOrFail($id);
    
            // Définition des règles de validation
            $rules = [
                'first_name' => 'required|regex:/^[a-zA-ZÀ-ÿ -]+$/',
                'last_name' => 'required|regex:/^[a-zA-ZÀ-ÿ -]+$/',
                'phone' => 'required|regex:/^[+0-9 -]+$/|between:10,20',
                'email' => 'required|email|unique:users,email,' . $user->id,
                'password' => 'nullable|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
            ];
    
            $customMessages = [
                'first_name.required' => 'Le champ Prénom est requis.',
                'first_name.regex' => 'Le Prénom doit contenir  des lettres, les espaces et les tirets sont acceptés.',
    
                'last_name.required' => 'Le champ Nom est requis.',
                'last_name.regex' => 'Le Nom doit contenir uniquement des lettres.',
    
                'phone.required' => 'Le champ Téléphone est requis.',
                'phone.min' => 'Le Téléphone doit contenir au moins 10 chiffres',
                'phone.min' => 'Le Téléphone doit contenir au maximum 20 chiffres',
                'phone.regex' => 'Le Téléphone doit contenir uniquement des chiffres.',
    
                'email.required' => 'Le champ Email est requis.',
                'email.email' => 'Veuillez saisir une adresse email valide.',
                'email.unique' => 'Cet email est déjà pris.',
    
                'password.min' => 'Le mot de passe doit comporter au moins 8 caractères.',
                'password.regex' => 'Le mot de passe doit contenir au moins une majuscule, une minuscule et un chiffre.',
            ];
            // Validation des données du formulaire
            $request->validate($rules, $customMessages);
            // Utilisation d'une transaction pour assurer la cohérence des mises à jour
            DB::transaction(function () use ($request, $user) {
                // Mise à jour des informations de l'utilisateur dans la table informations
                $user->information->update([
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                ]);
                // Mise à jour de l'email dans la table users
                $user->email = $request->input('email');
                // Mise à jour du mot de passe s'il est fourni
                if ($request->filled('password')) {
                    $user->password = bcrypt($request->input('password'));
                }
                $user->save();
            });
    
            // // Mise à jour du conseiller s'il est fourni
            // if ($request->filled('conseiller_id')) {
            //     // Mettez à jour l'entrée dans la table pivot
            //     ConseillerJeune::where('jeune_id', $user->id)
            //         ->update(['conseiller_id' => $request->input('conseiller_id')]);
            // }
            return redirect()->route('conseiller.user.user')->with('success', 'Mis à jour faite avec succès.');
        }
    
        public function destroy_manager($id)
        {
            $user = User::find($id);
            Log::info($user);
            if ($user) {
                $user->delete();
                return redirect()->route('conseiller.user.user')->with('success', 'Formation supprimée avec succès.');
            } else {
                return redirect()->route('conseiller.user.user')->with('error', 'Formation non trouvée.');
            }
        }

    public function jeunes()
    {
        // Récupérez l'utilisateur connecté (conseiller)
        $conseiller = auth()->user();
        $jeunes = $conseiller->jeunes;

        return view('conseiller.jeune', compact('jeunes'));
    }
    public function formation()
    {
        $formations = Training::all();

        // Récupérer le nombre de candidatures pour chaque formation selon le statut
        $candidaturesParFormation = [];

        foreach ($formations as $formation) {
            // Récupérer le nombre de candidatures 'en attente' pour chaque formation
            $candidaturesEnAttente = UserTraining::where('training_id', $formation->id)
                ->where('status', 'en attente')
                ->count();

            // Récupérer le nombre de candidatures 'acceptées' pour chaque formation
            $candidaturesAcceptees = UserTraining::where('training_id', $formation->id)
                ->where('status', 'accepte')
                ->count();

            // Stocker le nombre de candidatures 'en attente' et 'acceptées' par formation dans un tableau associatif
            $candidaturesParFormation[$formation->id]['en_attente'] = $candidaturesEnAttente;
            $candidaturesParFormation[$formation->id]['accepte'] = $candidaturesAcceptees;
        }

        return view('conseiller.trainings.training', compact('formations', 'candidaturesParFormation'));
    }

    public function showCandidatures(Training $formation)
    {
        // Récupérer les candidatures pour la formation spécifique avec le statut en attente
        $candidaturesEnAttente = $formation->userTrainings()
            ->where('status', 'en attente')
            ->with('user.information')
            ->get();

        return view('conseiller.trainings.showCandidature', compact('formation', 'candidaturesEnAttente'));
    }

    public function validerCandidature(UserTraining $candidature)
    {
        // Valider la candidature
        $candidature->update(['status' => 'accepte']);
    

        return redirect()->back()->with('success', 'Candidature validée avec succès.');
    }

    public function refuserCandidature(UserTraining $candidature)
    {
        // Refuser la candidature
        $candidature->update(['status' => 'refuse']);
    

        return redirect()->back()->with('success', 'Candidature refusée avec succès.');
    }

    public function showAcceptees($formationId)
    {
        // Récupérer la formation
        $formation = Training::findOrFail($formationId);

        // Récupérer les utilisateurs acceptés pour cette formation
        $usersAcceptes = UserTraining::where('training_id', $formation->id)
            ->where('status', 'accepte')
            ->with(['user.information', 'user.conseillerJeunes.conseiller.information'])
            ->get();

        return view('conseiller.trainings.showAccepte', compact('formation', 'usersAcceptes'));
    }

    public function atelier()
    {
        $conseiller = auth()->user();
        $jeunesSuivisIds = $conseiller->jeunes->pluck('id')->toArray();

        $ateliers = Atelier::all();
        $candidaturesParAtelier = [];

        foreach ($ateliers as $atelier) {
            $candidaturesEnAttente = UserAtelier::whereIn('user_id', $jeunesSuivisIds)
                ->where('atelier_id', $atelier->id)
                ->where('status', 'en attente')
                ->count();

                $candidaturesAcceptees = UserAtelier::where('atelier_id', $atelier->id)
                ->where('status', 'accepte')
                ->count();
            $candidaturesParAtelier[$atelier->id]['en_attente'] = $candidaturesEnAttente;
            $candidaturesParAtelier[$atelier->id]['accepte_total'] = $candidaturesAcceptees;
        }

        return view('conseiller.ateliers.atelier', compact('ateliers', 'candidaturesParAtelier'));
    }

    public function enAttente(Atelier $atelier)
    {
        $conseiller = auth()->user();
        $jeunesSuivisIds = $conseiller->jeunes->pluck('id')->toArray();

        $candidaturesEnAttente = UserAtelier::whereIn('user_id', $jeunesSuivisIds)
            ->where('atelier_id', $atelier->id)
            ->where('status', 'en attente')
            ->with('user.information') // Charger les informations utilisateur
            ->get();

        return view('conseiller.ateliers.showCandidature', compact('candidaturesEnAttente', 'atelier'));
    }
    public function accepter($id)
    {
        // Trouver la candidature
        $candidature = UserAtelier::findOrFail($id);

        // Modifier le statut de la candidature en 'accepte'
        $candidature->status = 'accepte';
        $candidature->save();

        return redirect()->back()->with('success', 'Candidature acceptée avec succès.');
    }

    public function refuser($id)
    {
        // Trouver la candidature
        $candidature = UserAtelier::findOrFail($id);

        // Modifier le statut de la candidature en 'refuse'
        $candidature->status = 'refuse';
        $candidature->save();

        return redirect()->back()->with('success', 'Candidature refusée.');
    }


    public function generatePDF($id)
    {
        $user = User::find($id);
        $information = Information::where('user_id', $id)->first();
        $formattedDate = now()->format('d/m/Y');
        
        if ($user && $information) {
            // Calcul de l'âge à partir de la date de naissance
            $dateOfBirth = \Carbon\Carbon::parse($information->dateOfBirth);
            $age = $dateOfBirth->diffInYears(\Carbon\Carbon::now());

            // Nom du fichier PDF avec l'âge inclus
            $fileName = 'Dossier_inscription_' . $information->last_name . '_' . $information->first_name . '_' . $age . 'ans_' . $formattedDate . '.pdf';

            // Générer le contenu du PDF
            $pdf = \PDF::loadView('user_info', [
                'user' => $user,
                'information' => $information,
                'age' => $age, // Passer l'âge à la vue
            ]);

            // Télécharger le fichier PDF
            return $pdf->download($fileName);
        } else {
            return "Utilisateur introuvable ou manque d'informations.";
        }
    }

}
