<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Session\TokenMismatchException;
use App\Exceptions\AuthenticationFailedException;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            if ($request->wantsJson()) {
                return response()->json(['error' => 'Not Found'], 404);
            } else {
                return response()->view('errors.404', [], 404);
            }
        } elseif ($exception instanceof TokenMismatchException) {
            if ($request->wantsJson()) {
                return response()->json(['error' => 'Token mismatch'], 419);
            } else {
                return response()->view('errors.419', [], 419);
            }
        } elseif ($exception instanceof QueryException && $exception->getCode() == 2002) {
            if ($request->wantsJson()) {
                return response()->json(['error' => 'Erreur de connexion à la base de données. Veuillez réessayer plus tard'], 500);
            } else {
                return response()->view('errors.2002', [], 500);
            }
        }elseif ($exception instanceof AuthenticationFailedException) {
            if ($request->wantsJson()) {
                return response()->json(['error' => "Informations d'identification invalides. Veuillez contacter l'administrateur."], 401);
            } else {
                return response()->view('errors.401', [], 401);
            }
        }

        // Passer l'exception à la vue personnalisée
        if ($request->wantsJson()) {
            return response()->json(['error' => 'Une erreur est survenue. Veuillez réessayer.'], 500);
        } else {
            return response()->view('errors.error', ['exception' => $exception], 500);
        }
    }
}
