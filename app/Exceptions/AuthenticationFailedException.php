<?php

namespace App\Exceptions;

use Exception;

class AuthenticationFailedException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        if ($request->wantsJson()) {
            return response()->json(['error' => "Informations d'identification invalides. Veuillez contacter l'administrateur."], 401);
        } else {
            return response()->view('errors.401', [], 401);
        }
        
    }
}
