# Mission Locale API
 
Ce guide détaille les étapes pour déployer le projet Laravel sur le serveur OVH.
 
## Étapes
 
1. **Connexion au Serveur OVH via SSH**
 
    ```bash
    ssh utilisateur@ssh.cluster.hosting.ovh.net
    ```
    Remplacez `utilisateur` par `le nom OVH`.
2. **Initialiser le Projet**
 
    ```bash
    git init
    ```
 
3. **Connexion au Dépôt Git**
 
    Assurez-vous d'avoir configuré vos clés SSH sur le serveur et dans votre compte Git.
 
    ```bash
    git clone git@github.com:utilisateur/missionlocale.git
    ```
 
4. **Générer les Clés SSH de Git**
 
    Si vous n'avez pas encore configuré vos clés SSH sur le serveur :
 
    ```bash
    ssh-keygen -t rsa -b 4096 -C "email@email.com"
    ```
 
    Ajoutez ensuite la clé générée au compte compte Git.
 
5. **Cloner le Projet**
 
    ```bash
    cd missionlocale
    ```
6. **Installer les Dépendances avec Composer**
 
    ```bash
    php composer.phar install
    ```
 
7. **Configuration du Fichier .env**
 
    Copiez le fichier `.env.example` en `.env` et configurez les variables d'environnement comme votre base de données, etc.
 
    ```bash
    cp .env.example .env
    nano .env # ou utilisez votre éditeur de texte préféré
    ```
 
8. **Générer les Clés d'Application**
 
    ```bash
    php artisan key:generate
    ```
 
9. **Créer le Lien Symbolique vers le Répertoire de Stockage**
 
    ```bash
    php artisan storage:link
    ```
 
9. **Vider le Cache**
 
    ```bash
    php artisan cache:clear
    ```
## Liens Utiles
 
- [Lien officiel du site :](https://missionlocale-lesmureaux.fr/)
## [Retour en Haut](#mise-en-ligne-du-projet)
 
