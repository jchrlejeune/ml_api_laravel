<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('conseiller_jeune', function (Blueprint $table) {
            //
            Schema::table('conseiller_jeune', function (Blueprint $table) {
                $table->text('notifications')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('conseiller_jeune', function (Blueprint $table) {
            //
            Schema::table('conseiller_jeune', function (Blueprint $table) {
                $table->dropColumn('notifications');
            });
        });
    }
};
