<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AppointmentEntrepriseController;
use App\Http\Controllers\AtelierGestion;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BusinessManagerController;
use App\Http\Controllers\CandidatController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Controllers
use App\Http\Controllers\EntrepriseController;
use App\Http\Controllers\FrontController;


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Register entreprise route
Route::post('/register-entreprise', [EntrepriseController::class, 'register']);

// Register candidat route
Route::post('/registerCandidat', [CandidatController::class, 'register']);


// Log in Entreprise/Jeune route
Route::post('/login/entreprise-jeune', [AuthController::class, 'jeuneLogin']);

// // Appointement Entreprise
// Route::post('businessManager/rendez-vous', [AppointmentEntrepriseController::class, 'store']);
// Route::post('businessManager/calendrier', [AppointmentEntrepriseController::class, 'calendar']);


// AtelierGestion

// Route::post('atelier', [AtelierGestion::class, 'store']);


//BussinessManager                                                                                                                                                                                                                                                                                                                                                                                                                                     

// Route::post('businessManager', [BusinessManagerController::class, 'store']);


// FrontController
// Home
Route::get('/home', [FrontController::class, 'homeFront']);

Route::get('/', [FrontController::class, 'inscriptionjeune']);

Route::middleware('auth:sanctum')->group(function () {
    // espacepersoDetail
    Route::get('/espacepersoDetail', [FrontController::class, 'espacepersoDetail']);
    Route::get('/userdata', [FrontController::class, 'getUserData']);
});





Route::group([], function () {
    // Your API routes here without any middleware

    //inscription atelier
    Route::post('/atelier/{id}/inscription', [FrontController::class, 'inscription']);
    //postuler formation
    Route::post('/formations/{id}/postuler', [FrontController::class, 'postuler']);
    //postuleroffre
    Route::post('/emploi/{id}/postulerOffre', [FrontController::class, 'postulerOffre']);
    

    Route::post('/soumettre-formulaire', [FrontController::class, 'notifierConseillerFormulaire'])->name('soumettre-formulaire');

    Route::post('/soumettre-joboffer', [FrontController::class, 'notifierBusinessManagerFormulaire'])->name('soumettre-joboffer');
});

// actualite
Route::get('/actualite', [FrontController::class, 'actualite']);
Route::get('/actualite/{id}', [FrontController::class, 'show']);

// atelier
Route::get('/ateliers', [FrontController::class, 'ateliers']);
Route::get('/ateliers/{id}', [FrontController::class, 'atelierDetails']);

// formation
Route::get('/formations', [FrontController::class, 'formation']);
Route::get('/formations/{id}', [FrontController::class, 'formationDetail']);

//emploi
Route::get('/emploi', [FrontController::class, 'emploi']);

//gouvernance
Route::get('/gouvernance', [FrontController::class, 'gouvernance']);

//equipe
Route::get('/equipe', [FrontController::class, 'equipe']);