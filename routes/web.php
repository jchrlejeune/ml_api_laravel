<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AppointmentEntrepriseController;
use App\Http\Controllers\AtelierGestion;
use Illuminate\Support\Facades\Route;

// Pour le back
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TemplateBackController;
use App\Http\Controllers\EntrepriseController;
use App\Http\Controllers\CandidatController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\ValidationRegisterController;
use App\Http\Controllers\UserGestion;
use App\Http\Controllers\EntrepriseGestion;
use App\Http\Controllers\StaffGestion;
use App\Http\Controllers\NewsGestion;
use App\Http\Controllers\TrainingGestion;
use App\Http\Controllers\JobGestion;
use App\Http\Controllers\JobGestionBusinessManager;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\ConseillerController;
use App\Http\Controllers\ChargeEntrepriseController;
use App\Http\Controllers\BusinessManagerController;
use App\Http\Controllers\SendDocController;
use App\Http\Controllers\PhotoGroupeController;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('welcome');
// Route::fallback(function () {
//     return redirect()->route('home');
// });

Route::controller(TemplateBackController::class)->group(function(){
    Route::get('/admin','index')->name('admin.templateBack');
    Route::post('/logout','logout')->name('admin.logout');

    Route::get('/admin/notifications','showNotifications')->name('admin.showNotifications');
});


Route::controller(AuthController::class, 'auth')->group(function(){
    Route::get('/login/entreprise', 'showEntrepriseLoginForm')->name('auth.entreprise_login');
    Route::post('/login/entreprise', 'entrepriseLogin')->name('auth.entreprise_login.post');
    Route::get('/login/jeune', 'showJeuneLoginForm')->name('auth.jeune_login');
    Route::post('/login/jeune', 'jeuneLogin')->name('auth.jeune_login.post');
});

Route::controller(ProfilController::class)->group(function(){
    Route::get('/profil','profil')->name('profil');
    Route::get('/profil/edit','edit')->name('edit');
    Route::put('/profil/edit','update')->name('update');
    Route::get('/profil/edit-password','editPassword')->name('editPassword');
    Route::put('/profil/edit-password','updatePassword')->name('updatePassword');
    Route::get('/profil/rendez-vous','rendezVous')->name('mesrendezvous');
    Route::get('/profil/mesformations','mesformations')->name('mesformations');
    Route::get('/profil/downloadDocument/{documentId}','downloadDocument')->name('downloadDocument');

});


Route::controller(EntrepriseController::class)->group(function () {
    Route::get('/admin/registerEntreprise','index')->name('admin.registerEntreprise');
    Route::post('/admin/registerEntreprise', 'register')->name('admin.registerEntreprise.post');
});

Route::controller(CandidatController::class)->group(function () {
    Route::get('/admin/registerCandidat','index')->name('admin.registerCandidat');
    Route::post('/admin/registerCandidat', 'register')->name('admin.registerCandidat.post');
    Route::post('/admin/user/registerCandidat', 'register_admin')->name('admin.user.registerCandidat.post');
});

Route::controller(ValidationRegisterController::class)->group(function ()
{
    Route::get('/admin/validPreregister','index')->name('admin.validPreregister');
    Route::post('/admin/validPreregister/jeune','validation')->name('admin.validPreregister.post');
    Route::post('/admin/validPreregister/entreprise','validationEntreprise')->name('admin.validPreregister.valid');
    Route::get('/admin/validPreregisterEntreprise','entreprise')->name('admin.validPreregisterEntreprise');
    Route::get('/admin/validPreregister/user_details','getUserDetails')->name('admin.validPreregister.details');
    Route::delete('/admin/validPreregister/deletejeune/{id}','deleteJeune')->name('admin.validPreregister.deletejeune');
    Route::delete('/admin/validPreregister/deleteentreprise/{id}','deleteentreprise')->name('admin.validPreregister.deleteentreprise');
    Route::post('/admin/validPreregister/{id}/upgradeentreprise','upgradeValid')->name('admin.validPreregister.upgrade');
    Route::post('/admin/validPreregister/{id}/upgradejeune','upgradeValidjeune')->name('admin.validPreregister.upgradejeune');
});


Route::controller(UserGestion::class)->group(function () {
    Route::get('/admin/users','index')->name('admin.user.user');
    Route::get('/admin/user/create','create')->name('admin.user.create');
    Route::get('/admin/user/edit/{id}', 'edit')->name('admin.user.edit_user');
    Route::put('/admin/user/edit/update/{id}', 'update')->name('admin.user.update');
    Route::delete('/admin/user/destroy/{id}', 'destroy')->name('admin.user.destroy');
    Route::put('/admin/user/toggle-activation/{id}','toggleActivation')->name('admin.toggleActivation');
});


Route::controller(EntrepriseGestion::class)->group(function () {
    Route::get('/admin/companies','index')->name('admin.company.company');
    Route::get('/admin/companies/create','create')->name('admin.company.create');
    Route::get('/admin/companies/edit/{id}', 'edit')->name('admin.company.edit_user');
    Route::put('/admin/companies/edit/update/{id}', 'update')->name('admin.company.update');
    Route::delete('/admin/companies/destroy/{id}', 'destroy')->name('admin.company.destroy');

});

Route::controller(ManagerController::class)->group(function (){
    Route::get('admin/manager','index')->name('admin.manager.manager');
    Route::get('admin/manager/inactif','managerInactif')->name('admin.manager.managerInactive');
    Route::get('admin/manager/create','create')->name('admin.manager.create');
    Route::post('/admin.manager','store')->name('admin.manager.store');
    Route::get('/admin/manager/{id}/toggle','toggle')->name('admin.manager.toggle');
    Route::get('/admin/manager/edit/{id}', 'edit')->name('admin.manager.edit_manager');
    Route::put('admin/manager/edit/update/{id}','update')->name('admin.manager.update');
});

Route::controller(BusinessManagerController::class)->group(function (){
    Route::get('admin/businessManager','index')->name('admin.businessManager.manager');
    Route::get('admin/businessManager/inactif','businessManagerInactif')->name('admin.businessManager.businessManagerInactive');
    Route::get('admin/businessManager/create','create')->name('admin.businessManager.create');
    Route::post('/admin.businessManager','store')->name('admin.businessManager.store');
    Route::get('/admin/businessManager/{id}/toggle','toggle')->name('admin.businessManager.toggle');
    Route::get('/admin/businessManager/edit/{id}', 'edit')->name('admin.businessManager.edit_manager');
    Route::put('admin/businessManager/edit/update/{id}','update')->name('admin.businessManager.update');
});

Route::controller(StaffGestion::class)->group(function () {
    Route::get('/admin/staff','index')->name('admin.staff.staff');
    Route::get('/admin/staff/create','create')->name('admin.staff.create_staff');
    Route::post('/admin.staff','store')->name('admin.staff.store');
    Route::get('/admin/staff/edit/{id}','edit')->name('admin.staff.staff.edit');
    Route::put('admin.staff/edit/update/{id}','update')->name('admin.staff.update');
    Route::delete('/admin/staff/{id}','staffdelete')->name('admin.staff.delete');
});

Route::controller(NewsGestion::class)->group(function () {
    Route::get('/admin/news','index')->name('admin.news.news');
    Route::get('/admin/news/create','create')->name('admin.news.create');
    Route::post('/admin.news','store')->name('admin.news.store');
    Route::get('/admin/news/edit/{id}', 'edit')->name('admin.news.edit');
    Route::put('admin.news/edit/update/{id}','update')->name('admin.news.update');
    Route::delete('/admin/news/delete/{id}', 'destroy')->name('admin.news.destroy');
});

Route::controller(PhotoGroupeController::class)->group(function () {
    Route::get('/admin/photogroupe','index')->name('admin.photogroupe.photogroupe');
    Route::get('/admin/photogroupe/create','create')->name('admin.photogroupe.create');
    Route::post('/admin.photogroupe','store')->name('admin.photogroupe.store');
    Route::get('/admin/photogroupe/edit/{id}', 'edit')->name('admin.photogroupe.edit');
    Route::put('admin.photogroupe/edit/update/{id}','update')->name('admin.photogroupe.update');
    Route::delete('/admin/photogroupe/delete/{id}', 'destroy')->name('admin.photogroupe.destroy');
});

Route::controller(TrainingGestion::class)->group(function () {
    Route::get('/admin/training','index')->name('admin.training.training');
    Route::get('/admin/training/trainingInactive','inactiveView')->name('admin.training.traininginactive');
    Route::get('/admin/training/create','create')->name('admin.training.create');
    Route::post('/admin.training','store')->name('admin.training.store');
    Route::get('/admin/training/{id}/toggle','toggle')->name('admin.training.toggle');
    Route::get('/admin/training/edit/{id}', 'edit')->name('admin.training.edit_training');
    Route::put('admin/training/edit/update/{id}','update')->name('admin.training.update');
    Route::delete('/admin/training/delete/{id}', 'destroy')->name('admin.training.destroy');
});

Route::controller(JobGestion::class)->group(function () {
    Route::get('/admin/job','index')->name('admin.job.job');
    Route::get('/admin/alljob','alljob')->name('admin.job.alljob');
    Route::get('/admin/job/create','create')->name('admin.job.create');
    Route::post('/admin.job','store')->name('admin.job.store');
    Route::get('/admin/job/{id}/toggle','toggle')->name('admin.job.toggle');
    Route::get('/admin/job/edit/{id}', 'edit')->name('admin.job.edit_job');
    Route::put('admin/job/edit/update/{id}','update')->name('admin.job.update');
    Route::delete('admin/job/alljob/delete/{id}','delete')->name('admin.job.delete');
});

Route::controller(AtelierGestion::class)->group(function () {
    Route::get('/admin/atelier','index')->name('admin.atelier.atelier');
    Route::get('/admin/atelier/atelierInactive','inactiveView')->name('admin.atelier.atelierinactive');
    Route::get('/admin/atelier/create','create')->name('admin.atelier.create');
    Route::post('/admin.atelier','store')->name('admin.atelier.store');
    Route::get('/admin/atelier/{id}/toggle','toggle')->name('admin.atelier.toggle');
    Route::get('/admin/atelier/edit/{id}', 'edit')->name('admin.atelier.edit');
    Route::put('admin/atelier/edit/update/{id}','update')->name('admin.atelier.update');
    Route::delete('admin/atelier/delete/{id}','delete')->name('admin.atelier.delete');
});


Route::controller(ConseillerController::class)->group(function () {
    Route::get('/conseiller','index')->name('conseiller.user.user');
    Route::get('/conseiller/user/create','create_manager')->name('conseiller.user.create');
    Route::get('/conseiller/user/edit/{id}', 'edit_manager')->name('conseiller.user.edit_user');
    Route::put('/conseiller/user/edit/update/{id}', 'update_manager')->name('conseiller.user.update');
    Route::delete('/conseiller/user/destroy/{id}', 'destroy_manager')->name('conseiller.user.destroy');
    Route::put('/conseiller/user/toggle-activation/{id}','toggleActivation_manager')->name('conseiller.toggleActivation');
    Route::get('/conseiller/formations','formation')->name('conseiller.trainings.training');
    Route::get('/conseiller/formations/{formation}/candidature','showCandidatures')->name('conseiller.trainings.showCandidature');
    Route::post('/candidatures/{candidature}/valider','validerCandidature')->name('formations.validerCandidature');
    Route::post('/candidatures/{candidature}/refuser','refuserCandidature')->name('formations.refuserCandidature');
    Route::get('/candidatures/{formation}/acceptees','showAcceptees')->name('conseiller.trainings.showAccepte');
    Route::get('/conseiller/ateliers','atelier')->name('conseiller.ateliers.atelier');
    Route::get('/conseiller/ateliers/{atelier}/en-attente','enAttente')->name('conseiller.ateliers.showCandidature');
    Route::post('/conseiller/ateliers/{id}/accepter','accepter')->name('conseiller.ateliers.accepter');
    Route::post('/conseiller/ateliers/{id}/refuser','refuser')->name('conseiller.ateliers.refuser');
    Route::get('/conseiller/generate-pdf/{id}','generatePDF')->name('generatePDF');
});

Route::controller(AppointmentController::class)->group(function () {
    Route::get('/conseiller/rendez-vous','index')->name('conseiller.appointments.index');
    Route::get('/conseiller/rendez-vous/{jeuneId}/create','create')->name('conseiller.appointments.create');
    Route::post('/conseiller/rendez-vous','store')->name('conseiller.appointments.store');
    Route::delete('/conseiller/rendez-vous/{id}/cancel','cancel')->name('conseiller.appointments.cancel');
    Route::get('/conseiller/calendrier','calendar')->name('conseiller.appointments.calendar');
});

Route::get('/conseiller/sendDoc', [SendDocController::class, 'index'])->name('conseiller.sendDoc.index');
Route::get('/conseiller/sendDoc/{jeuneId}/send', [SendDocController::class, 'send'])->name('conseiller.sendDoc.send');
Route::post('/conseiller/sendDoc/{jeuneId}/send', [SendDocController::class, 'store'])->name('conseiller.sendDoc.store');

Route::get('/conseiller/notifications', [NotificationController::class, 'index'])->name('conseiller.notification.index');
Route::delete('/conseiller/notifications/delete/{uuid}', [NotificationController::class, 'delete'])->name('conseiller.notification.delete');

Route::controller(AppointmentEntrepriseController::class)->group(function () {
    Route::get('/businessManager/rendez-vous','index')->name('businessManager.appointments.index');
    Route::get('/businessManager/rendez-vous/{entrepriseId}/create','create')->name('businessManager.appointments.create');
    Route::post('/businessManager/rendez-vous','store')->name('businessManager.appointments.store');
    Route::get('/businessManager/calendrier','calendar')->name('businessManager.appointments.calendar');
});



Route::controller(ChargeEntrepriseController::class)->group(function () {
    Route::get('/businessManager/entreprise','entreprise')->name('businessManager.entreprise');
});

Route::get('/businessManager/notifications', [NotificationController::class, 'index'])->name('businessManager.notification.index');
Route::delete('/businessManager/notifications/delete/{uuid}', [NotificationController::class, 'delete'])->name('businessManagerconseiller.notification.delete');

Route::controller(JobGestionBusinessManager::class)->group(function () {
    Route::get('/businessManager/job','index')->name('businessManager.job.job');
    Route::get('/businessManager/alljob','alljob')->name('businessManager.job.alljob');
    Route::get('/businessManager/job/create','create')->name('businessManager.job.create');
    Route::post('/businessManager.job','store')->name('businessManager.job.store');
    Route::get('/businessManager/job/{id}/toggle','toggle')->name('businessManager.job.toggle');
    Route::get('/businessManager/job/edit/{id}', 'edit')->name('businessManager.job.edit_job');
    Route::put('businessManager/job/edit/update/{id}','update')->name('businessManager.job.update');
    Route::delete('businessManager/job/alljob/delete/{id}','delete')->name('businessManager.job.delete');
});

Route::controller(FrontController::class)->group(function () {
    Route::get('/','inscriptionjeune')->name('home');
   // Route::get('/','home')->name('home');
    Route::get('/nosmissions','nosmissions')->name('nosmissions');
    Route::get('/etreAccompagne','etreAccompagne')->name('etreAccompagne');
    Route::get('/noscommunes','noscommunes')->name('noscommunes');
    Route::get('/orienter','orienter')->name('orienter');
    Route::get('/seformer','seformer')->name('seformer');
    Route::get('/trouverUnEmploi','TrouverUnEmploi')->name('trouverUnEmploi');
    Route::get('/actualite','actualite')->name('actualite');
    Route::get('/actualité/{id}','show')->name('actualiteDetail');

    Route::get('/ateliers','ateliers')->name('ateliers');
    Route::get('/ateliers/{id}','atelierDetails')->name('atelierDetail');
    Route::post('/ateliers/inscription/{atelier}','inscription')->name('ateliers.inscription');

    Route::get('/notreexpertise','notreexpertise')->name('notreexpertise');
    Route::get('/taxeapprentissage','taxeapprentissage')->name('taxeapprentissage');
    Route::get('/demarcheRSE','demarcheRSE')->name('demarcheRSE');

    
    Route::get('/espacepersonneljeune','espacepersonneljeune')->name('espacepersonneljeune');
    Route::get('/espaceentreprise','espaceentreprise')->name('espaceentreprise');
    Route::get('/inscriptionjeune','inscriptionjeune')->name('inscriptionjeune');
    Route::get('/inscriptionentreprise','inscriptionentreprise')->name('inscriptionentreprise');
    Route::get('/preinscription','preinscription')->name('preinscription');

   
    Route::get('/nosformations','formation')->name('formation');
    Route::get('/nosformation/{id}','formationDetail')->name('formationDetail');
    Route::post('/nosformations/postuler/{formation}','postuler')->name('formations.postuler');

    Route::get('/nosoffresemploi','emploi')->name('emploi');
    Route::post('/nosoffresemploi/postuler/{offreId}','postulerOffre')->name('postulerOffre');

    Route::get('/gouvernance','gouvernance')->name('gouvernance');

    Route::get('/notreequipe','equipe')->name('equipe');
    
    Route::get('/espacepersonnel','espacepersonnel')->name('espacepersonnel');

    
    Route::get('/espacepersoDetail','espacepersoDetail')->name('espacepersoDetail');
    Route::post('/soumettre-formulaire','notifierConseillerFormulaire')->name('soumettreFormulaire');

    Route::get('/contact','contact')->name('contact');

    Route::get('/espaceentrepriseDetail','espaceentrepriseDetail')->name('espaceentrepriseDetail');

    // Route::get('/userdata','getUserData')->name('userdata');

});

